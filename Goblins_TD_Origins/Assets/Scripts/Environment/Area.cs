using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Area : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Color previewTowerHoverColor;
    [SerializeField] private Color hoverColor;
    [SerializeField] private Vector3 posOffset;

    private Color originalColor;
    private Color originalPreviewTowerColor;
    private Color originalRangeCircleColor;
    private Renderer rend;

    private GameObject tower;
    private GameObject previewTower;

    private BuildManager buildManager;
    private Shop shop;

    private void Start()
    {
        rend = GetComponent<Renderer>();
        originalColor = rend.material.color;
        buildManager = BuildManager.Instance;
        shop = Shop.Instance;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        TowerCurrentStatsManager.Instance.ShowOrHideTowerCurrentStats(null);

        if (buildManager.GetTowerToUpgrade())
        {
            BuildManager.Instance.SetTowerToUpgrade(null, null);
        }

        if (tower == null && shop.BuyTower())
        {
            PlaceTower();
            if (GetComponent<DialogueTrigger>())
            {
                GetComponent<DialogueTrigger>().TriggerDialogue();
            }
        }
    }

    private void PlaceTower()
    {
        Vector3 newPos = transform.position + posOffset;
        PreviewTowerColorHandler(originalPreviewTowerColor, true);
        if (previewTower.GetComponent<ShootTower>())
        {
            previewTower.GetComponent<ShootTower>().enabled = true;
        }
        else if (previewTower.GetComponent<PulseTower>())
        {
            previewTower.GetComponent<PulseTower>().enabled = true;
        }
        previewTower.GetComponent<CapsuleCollider>().enabled = true;
        previewTower.transform.GetChild(0).gameObject.SetActive(false);
        previewTower.transform.position = newPos;
        tower = previewTower;
        previewTower = null;

        rend.material.color = originalColor;

        buildManager.SetTowerToBuild(null);

        TowerListHandler.Instance.ResetTowerDisplayName();
        ResetTowerImages();
    }

    private void ResetTowerImages()
    {
        Image[] towerImages = TowerListHandler.Instance.AllTowers;

        for (int i = 0; i < towerImages.Length; i++)
        {
            towerImages[i].color = Color.white;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (tower && !tower.activeSelf)
        {
            tower = null;
        }

        TowerBase towerToBuild = buildManager.GetTowerToBuild();

        if (tower == null && towerToBuild && previewTower == null)
        {
            rend.material.color = hoverColor;
            ShowPreviewTower(towerToBuild.TowerInfo.TowerKey);
        }
    }

    private void ShowPreviewTower(string turretKey)
    {
        Vector3 newPos = transform.position + posOffset;
        previewTower = ObjectPooler.Instance.GetPoolObject(turretKey);
        if (previewTower.GetComponent<ShootTower>())
        {
            previewTower.GetComponent<ShootTower>().enabled = false;
        }else if (previewTower.GetComponent<PulseTower>())
        {
            previewTower.GetComponent<PulseTower>().enabled = false;
        }
        previewTower.transform.GetChild(0).gameObject.SetActive(true);
        PreviewTowerColorHandler(previewTowerHoverColor, false);
        previewTower.GetComponent<CapsuleCollider>().enabled = false;
        previewTower.transform.position = newPos;
    }

    private void PreviewTowerColorHandler(Color c, bool toOriginal)
    {
        Renderer[] renderers = previewTower.GetComponentsInChildren<Renderer>();

        for (int i = 0; i < renderers.Length; i++)
        {
            if (i == 0)
            {
                ChangeColorRangeCircle(renderers[i], c, toOriginal);
            }
            else
            {
                ChangeColorPreviewTower(renderers[i], c, toOriginal);
            }
        }
    }

    private void ChangeColorRangeCircle(Renderer renderer, Color c, bool toOriginal)
    {
        if (toOriginal)
        {
            renderer.material.color = originalRangeCircleColor;
        }
        else
        {
            originalRangeCircleColor = renderer.material.color;
            renderer.material.color = c;
        }
    }

    private void ChangeColorPreviewTower(Renderer renderer, Color c, bool toOriginal)
    {
        if (toOriginal)
        {
            renderer.material.color = c;
        }
        else
        {
            originalPreviewTowerColor = renderer.material.color;
            renderer.material.color = c;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        rend.material.color = originalColor;

        if (previewTower != null)
        {
            ObjectPooler.Instance.ReturnPoolObject(buildManager.GetTowerToBuild().TowerInfo.TowerKey, previewTower);
            PreviewTowerColorHandler(originalPreviewTowerColor, true);
            previewTower = null;
        }
    }

    public void SetUpgradedTower(GameObject upgradedTower)
    {
        tower = upgradedTower;
    }
}