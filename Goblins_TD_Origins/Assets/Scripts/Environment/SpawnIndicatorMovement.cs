using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnIndicatorMovement : MonoBehaviour
{
    private string _lightObjKey = "";
    private Transform target;
    private int waypointIndex;
    private float speed = 10f;
    private float arrivalDistance = 0.3f;
    private Transform[] routeWaypoints;

    public void Init(int routeIndex, string lightObjKey)
    {
        routeWaypoints = SpawnManager.Instance.Routes[routeIndex].RouteWaypoints;
        target = routeWaypoints[waypointIndex];
        _lightObjKey = lightObjKey;
    }

    private void OnEnable()
    {
        waypointIndex = 0;
    }

    private void Update()
    {
        MoveLight();
    }

    private void MoveLight()
    {
        Vector3 targetDirection = target.position - transform.position;
        transform.Translate(targetDirection.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= arrivalDistance)
        {
            MoveToNextWaypoint();
        }
    }

    private void MoveToNextWaypoint()
    {
        if (waypointIndex == routeWaypoints.Length - 1)
        {
            ObjectPooler.Instance.ReturnPoolObject(_lightObjKey, gameObject);
            return;
        }
        waypointIndex++;
        target = routeWaypoints[waypointIndex];
    }
}
