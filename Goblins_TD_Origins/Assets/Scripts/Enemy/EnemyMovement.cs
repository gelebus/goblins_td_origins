using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private Transform target;
    private int waypointIndex;
    private float speed = 1f;
    private float rotationSpeed = 3f;
    private float arrivalDistance = 0.3f;
    private Enemy enemy;
    private Transform[] routeWaypoints;

    public void Init(int routeIndex)
    {
        routeWaypoints = SpawnManager.Instance.Routes[routeIndex].RouteWaypoints;
        target = routeWaypoints[waypointIndex];
    }

    private void OnEnable()
    {
        waypointIndex = 0;
        enemy = GetComponent<Enemy>();
        speed = enemy.Speed;
    }


    private void Update()
    {
        if (!enemy.IsDead)
        {
            speed = enemy.Speed;
            MoveEnemy();
        }
    }

    private void MoveEnemy()
    {
        Vector3 targetDirection = target.position - transform.position;
        RotateEnemy(targetDirection);
        transform.Translate(targetDirection.normalized * speed * Time.deltaTime, Space.World);

        if(Vector3.Distance(transform.position, target.position) <= arrivalDistance)
        {
            MoveToNextWaypoint();
        }
    }

    private void RotateEnemy(Vector3 dir)
    {
        Vector3 rotation = new Vector3(dir.x, 0, dir.z);
        if(rotation == Vector3.zero)
        {
            return;
        }
        Quaternion LookRotation = Quaternion.LookRotation(rotation);
        transform.rotation = Quaternion.Lerp(transform.rotation, LookRotation, Time.deltaTime * rotationSpeed);
    }

    private void MoveToNextWaypoint()
    {
        if(waypointIndex == routeWaypoints.Length - 1)
        {
            ObjectPooler.Instance.ReturnPoolObject(enemy.EnemyKey, gameObject);
            SpawnManager.Instance.AllActiveEnemies.Remove(gameObject);
            if (!enemy.IsCamo)
            {
                SpawnManager.Instance.ActiveNormalEnemies.Remove(gameObject);
            }
            enemy.IsDead = true;
            PlayerManager.Instance.HeartAmount -= 1;
            return;
        }
        waypointIndex++;
        target = routeWaypoints[waypointIndex];
    }
}
