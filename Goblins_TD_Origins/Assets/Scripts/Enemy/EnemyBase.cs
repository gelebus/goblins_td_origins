using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "Enemy/Create a new enemy")]
public class EnemyBase : ScriptableObject
{
    [SerializeField] string enemyKey;
    [SerializeField] float standardSpeed;
    [SerializeField] int maxHp;
    [SerializeField] int maxArmor;
    [SerializeField] int lootAmount;
    [SerializeField] EnemyType enemyType;

    [Header("HpBar")]
    [SerializeField] float hpBarYOffset;
    [SerializeField] HpBarBase hpBar;

    public string EnemyKey
    {
        get { return enemyKey; }
    }
    public float StandardSpeed
    {
        get { return standardSpeed; }
    }
    public int MaxHp
    {
        get { return maxHp; }
    }
    public int MaxArmor
    {
        get { return maxArmor; }
    }
    public int LootAmount
    {
        get { return lootAmount; }
    }
    public EnemyType EnemyType
    {
        get { return enemyType; }
    }
    public float HpBarYOffset
    {
        get { return hpBarYOffset; }
    }
    public HpBarBase HpBar
    {
        get { return hpBar; }
    }
}

public enum EnemyType
{
    Normal,
    Camo,
    Heavy,
    Boss
}