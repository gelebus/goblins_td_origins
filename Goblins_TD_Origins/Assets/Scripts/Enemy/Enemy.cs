using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float Speed { get; private set; }

    private float standardSpeed;
    private float hp;
    private float maxHp;
    private float armor;
    private float maxArmor;
    private int lootAmount;
    public bool IsCamo { get; private set; }
    public bool IsHeavy { get; private set; }
    public bool IsBoss { get; private set; }
    public string EnemyKey { get; private set; }
    public bool IsDead { get; set; }

    private Animator anim;
    private List<DotEffect> dotEffects;
    private Dictionary<DotEffectName, float> dotEffectTimers;
    private float dotTimer = 0f;

    private void OnEnable()
    {
        IsDead = false;
    }

    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
        dotEffects = new List<DotEffect>();
        dotEffectTimers = new Dictionary<DotEffectName, float>();
    }
    private void Update()
    {
        if(hp <= 0 && !IsDead)
        {
            StartCoroutine(Die());
        }
        else if(dotEffects.Count > 0)
        {
            while(hp > 0 && dotTimer <= 0)
            {
                TakeDamageOverTime();
                for (int i = 0; i < dotEffects.Count; i++)
                {
                    if(dotEffectTimers[dotEffects[i].DotEffectName] <= 0)
                    {
                        dotEffects.Remove(dotEffects[i]);
                        i = -1;
                    }
                }
                dotTimer = 1;
            }
        }
        if(Speed < standardSpeed)
        {
            Speed = Mathf.Clamp(Speed += Time.deltaTime * 0.025f, 0f, standardSpeed);
        }
        dotTimer -= Time.deltaTime;
    }

    public void Init(EnemyBase enemy)
    {
        hp = enemy.MaxHp;
        maxHp = enemy.MaxHp;
        armor = enemy.MaxArmor;
        maxArmor = enemy.MaxArmor;
        standardSpeed = enemy.StandardSpeed;
        Speed = standardSpeed;
        lootAmount = enemy.LootAmount;
        EnemyKey = enemy.EnemyKey;
        IsCamo = enemy.EnemyType == EnemyType.Camo;
        IsHeavy = enemy.EnemyType == EnemyType.Heavy;
        IsBoss = enemy.EnemyType == EnemyType.Boss;
    }

    public void TakeDamage(int Damage)
    {
        if (armor <= 0)
        {
            hp -= Damage;
        }
        else
        {
            armor -= 1;
        }
    }

    public void Slow(float speedMultiplier,bool canSlowCamo , bool canSlowHeavy, bool canSlowBoss)
    {
        if(IsCamo && !canSlowCamo)
        {
            return;
        }
        else if (IsHeavy && !canSlowHeavy)
        {
            return;
        }else if (IsBoss && !canSlowBoss)
        {
            return;
        }
        Speed = speedMultiplier * standardSpeed;
    }

    private void TakeDamageOverTime()
    {
        foreach (DotEffect d in dotEffects)
        {
            if(hp <= 0)
            {
                return;
            }
            dotEffectTimers[d.DotEffectName] -= 1;
            hp -= d.DamageTotal / d.ExecutionTime;
            StartCoroutine(PlayDotEffect(d));
        }
    }

    private IEnumerator PlayDotEffect(DotEffect dotEffect)
    {
        GameObject impactEffect = ObjectPooler.Instance.GetPoolObject(dotEffect.DotEffectKey);
        impactEffect.transform.position = transform.position;
        impactEffect.transform.rotation = transform.rotation;
        yield return new WaitForSeconds(2f);
        ObjectPooler.Instance.ReturnPoolObject(dotEffect.DotEffectKey, impactEffect);
    }

    public void AddDotEffect(DotEffect dotEffect)
    {
        foreach (DotEffect d in dotEffects)
        {
            if (d.DotEffectName == dotEffect.DotEffectName)
            {
                dotEffectTimers[dotEffect.DotEffectName] = dotEffect.ExecutionTime;
                return;
            }
        }

        if (dotEffectTimers.ContainsKey(dotEffect.DotEffectName))
        {
            dotEffectTimers.Remove(dotEffect.DotEffectName);
        }
        dotEffectTimers.Add(dotEffect.DotEffectName, dotEffect.ExecutionTime);
        dotEffects.Add(dotEffect);
    }

    public float GetHpPercentage()
    {
        if (hp / maxHp < 0)
            return 0;

        return hp / maxHp;
    }
    public float GetArmorPercentage()
    {
        if (armor / maxArmor < 0)
            return 0;

        return armor/maxArmor;
    }

    private IEnumerator Die()
    {
        transform.tag = "Untagged";
        PlayerManager.Instance.MoneyAmount += lootAmount;
        anim.SetBool("IsDead", true);
        dotEffects.Clear();
        IsDead = true;
        SpawnManager.Instance.AllActiveEnemies.Remove(gameObject);
        if (!IsCamo)
        {
            SpawnManager.Instance.ActiveNormalEnemies.Remove(gameObject);
        }
        yield return new WaitForSeconds(4.75f);
        anim.SetBool("IsDead", false);
        ObjectPooler.Instance.ReturnPoolObject(EnemyKey, gameObject);
    }
}
