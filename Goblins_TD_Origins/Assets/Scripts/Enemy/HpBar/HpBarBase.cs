using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "Enemy/Create a healthbar")]
public class HpBarBase : ScriptableObject
{
    [SerializeField] Color hpColor;
    [Range(0.0001f, 0.0020f)]
    [SerializeField] float hpSize;

    [Header("armor (optional)")]
    [SerializeField] Color armorColor;
    [Range(0f, 1f)]
    [SerializeField] float armorSize;

    public Color HpColor
    {
        get { return hpColor; }
    }
    public float HpSize
    {
        get { return hpSize; }
    }
    public Color ArmorColor
    {
        get { return armorColor; }
    }
    public float ArmorSize
    {
        get { return armorSize; }
    }
}
