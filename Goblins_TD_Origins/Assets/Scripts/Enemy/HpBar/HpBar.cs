using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpBar : MonoBehaviour
{
    [SerializeField] private Transform hp;
    [SerializeField] private Transform armorBar;

    private Transform target;
    private Enemy enemy;
    private float speed;
    private float lastHpPercentage;
    private float lastArmorPercentage;
    private EnemyBase EnemyBase;

    private void Awake()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }
    public void Seek(Transform _target, EnemyBase _enemyBase)
    {
        target = _target;
        EnemyBase = _enemyBase;
        hp.GetComponent<Image>().color = EnemyBase.HpBar.HpColor;
        transform.GetChild(0).transform.localScale = new Vector3(EnemyBase.HpBar.HpSize, 0.0015f, EnemyBase.HpBar.HpSize);

        transform.position = new Vector3(target.position.x, target.position.y + EnemyBase.HpBarYOffset, target.position.z);
        enemy = target.gameObject.GetComponent<Enemy>();
        speed = enemy.Speed;
        lastHpPercentage = enemy.GetHpPercentage();

        if(EnemyBase.MaxArmor > 0)
        {
            armorBar.transform.GetComponent<Image>().color = EnemyBase.HpBar.ArmorColor;
            lastArmorPercentage = enemy.GetArmorPercentage();
        }
        else
        {
            lastArmorPercentage = 1;
        }
    }
    private void Update()
    {
        if(target == null)
        {
            return;
        }
        if (enemy.IsDead)
        {
            ObjectPooler.Instance.ReturnPoolObject("HpBar", gameObject);
        }
        if(lastHpPercentage != enemy.GetHpPercentage())
        {
            lastHpPercentage = enemy.GetHpPercentage();
            ChangeHpScale(lastHpPercentage);
        }
        if(lastHpPercentage != 1)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }
        if(lastArmorPercentage == 1)
        {
            armorBar.gameObject.SetActive(false);
        }
        if(EnemyBase.MaxArmor > 0)
        {
            if(lastArmorPercentage != enemy.GetArmorPercentage())
            {
                lastArmorPercentage = enemy.GetArmorPercentage();
                ChangeArmorScale(lastArmorPercentage);
            }
            if(lastArmorPercentage != 1)
            {
                transform.GetChild(0).gameObject.SetActive(true);
                armorBar.gameObject.SetActive(true);
            }
        }
        Move();
    }

    private void OnEnable()
    {
        ChangeHpScale(1);
        ChangeArmorScale(1);
    }

    private void Move()
    {
        Vector3 direction = new Vector3(target.position.x, target.position.y + EnemyBase.HpBarYOffset, target.position.z) - transform.position;
        float distanceThisFrame = Time.deltaTime * speed;
        transform.Translate(direction.normalized * distanceThisFrame, Space.World);
    }

    private void ChangeHpScale(float scale)
    {
        hp.transform.localScale = new Vector3(scale, 1f);
    }
    private void ChangeArmorScale(float scale)
    {
        if (armorBar == null || EnemyBase == null)
            return;

        armorBar.transform.localScale = new Vector3(scale * EnemyBase.HpBar.ArmorSize, 0.5f, 1f);
    }
}
