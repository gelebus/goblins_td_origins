using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveSpawner : MonoBehaviour
{
    [SerializeField] private int routeIndex;
    [SerializeField] private string lightObjKey;

    public Wave[] Waves;
    public bool SpawningWave { get; private set; }

    private ObjectPooler objectPooler;

    public IEnumerator SpawnWave(int waveIndex)
    {
        if(objectPooler == null)
        {
            objectPooler = ObjectPooler.Instance;
        }

        if(Waves[waveIndex].WaveGroups.Length > 0)
        {
            PlayWaveSpawnIndication();
        } 

        SpawningWave = true;
        for (int i = 0; i < Waves[waveIndex].WaveGroups.Length; i++)
        {
            yield return SpawnWaveGroup(Waves[waveIndex].WaveGroups[i]);
            yield return new WaitForSeconds(Waves[waveIndex].WaveGroups[i].TimeUntilNextWaveGroupInWave);
        }
        SpawningWave = false;
    }

    private IEnumerator SpawnWaveGroup(WaveGroup waveGroup)
    {
        for (int i = 0; i < waveGroup.AmountOfEnemies; i++)
        {
            SpawnEnemy(waveGroup.Enemy);
            yield return new WaitForSeconds(waveGroup.TimeBetweenEnemySpawns);
        }
    }

    private void SpawnEnemy(EnemyBase enemy)
    {
        GameObject clone = objectPooler.GetPoolObject(enemy.EnemyKey);
        clone.transform.position = SpawnManager.Instance.Routes[routeIndex].RouteWaypoints[0].position;
        clone.transform.rotation = SpawnManager.Instance.Routes[routeIndex].RouteWaypoints[0].rotation;
        clone.GetComponent<Enemy>().Init(enemy);
        clone.GetComponent<EnemyMovement>().Init(routeIndex);

        GameObject hpBar = objectPooler.GetPoolObject("HpBar");
        hpBar.GetComponent<HpBar>().Seek(clone.transform, enemy);

        SpawnManager.Instance.AllActiveEnemies.Add(clone);
        if (enemy.EnemyType != EnemyType.Camo)
        {
            SpawnManager.Instance.ActiveNormalEnemies.Add(clone);
        }
    }

    private void PlayWaveSpawnIndication()
    {
        GameObject lightEffect = ObjectPooler.Instance.GetPoolObject(lightObjKey);
        lightEffect.transform.position = SpawnManager.Instance.Routes[routeIndex].RouteWaypoints[0].position;
        lightEffect.GetComponent<SpawnIndicatorMovement>().Init(routeIndex, lightObjKey);
    }
}
