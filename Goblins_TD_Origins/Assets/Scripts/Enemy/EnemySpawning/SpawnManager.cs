using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private WaveSpawner[] waveSpawners;
    [SerializeField] private Route[] routes;

    [Header("Wave spawning")]
    public float TimeBetweenWaves = 5f;
    public float TimeBeforeFirstWave = 2f;

    [HideInInspector] 
    public bool LastWaveEnded;

    [HideInInspector]
    public List<GameObject> AllActiveEnemies;
    [HideInInspector]
    public List<GameObject> ActiveNormalEnemies;

    public Route[] Routes { get { return routes; } } 

    private float countdown;
    private int waveIndex;

    public static SpawnManager Instance { get; private set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }
        foreach (Route route in Routes)
        {
            Transform[] waypoints = new Transform[route.RouteParent.childCount];
            for (int i = 0; i < route.RouteParent.childCount; i++)
            {
                waypoints[i] = route.RouteParent.GetChild(i);
            }
            route.RouteWaypoints = waypoints;
        }
    }

    private void Start()
    {
        foreach (WaveSpawner waveSpawner in waveSpawners)
        {
            if (PlayerManager.Instance.MaxWave < waveSpawner.Waves.Length)
            {
                PlayerManager.Instance.MaxWave = waveSpawner.Waves.Length;
            }
        }
        countdown = TimeBeforeFirstWave;
        waveIndex = 0;
        AllActiveEnemies = new List<GameObject>();
        ActiveNormalEnemies = new List<GameObject>();
    }

    private void Update()
    {
        if(waveIndex > PlayerManager.Instance.MaxWave - 1)
        {
            LastWaveEnded = true;
            return;
        }

        if (!NextWave())
        {
            return;
        }

        if(countdown <= 0)
        {
            SpawnWaves();
            countdown = TimeBetweenWaves;
        }
        countdown -= Time.deltaTime;

        PlayerManager.Instance.CurrentWave = waveIndex;
    }
    private void SpawnWaves()
    {
        foreach(WaveSpawner waveSpawner in waveSpawners)
        {
            StartCoroutine(waveSpawner.SpawnWave(waveIndex));
        }
        waveIndex++;
    }

    private bool NextWave()
    {
        foreach(WaveSpawner waveSpawner in waveSpawners)
        {
            if (waveSpawner.SpawningWave)
            {
                return false;
            }
        }
        return true;
    }
}
