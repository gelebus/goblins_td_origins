using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveGroup
{
    [Space(5f)]
    public EnemyBase Enemy;
    public int AmountOfEnemies;
    [Space(15f)]
    public float TimeBetweenEnemySpawns = 0.4f;
    public float TimeUntilNextWaveGroupInWave = 6f;
}
