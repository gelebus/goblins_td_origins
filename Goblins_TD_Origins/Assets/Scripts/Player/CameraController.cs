using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("PanMovement")]
    [SerializeField] private float panSpeed = 15f;
    [SerializeField] private float panBorderSize = 10f;

    [Header("scrollMovement")]
    [SerializeField] private float scrollSpeed = 10f;
    [SerializeField] private Transform followPoint;
    [SerializeField] private float followSpeed = 10f;

    [Header("camera rotation while scrolling")]
    [SerializeField] private float turnSpeed = 10f;
    [SerializeField] private float maxXCameraRotation = 75f;
    [SerializeField] private float minXCameraRotation = 10f;

    [Header("Boundries")]
    [SerializeField] private float maxX = 35f;
    [SerializeField] private float minX = 5f;
    [SerializeField] private float maxZ = -10;
    [SerializeField] private float minZ = -30;
    [SerializeField] private float maxY = 30f;
    [SerializeField] private float minY = 10f;

    private Transform cameraPos;
    private bool pause;
    private float cameraMovementMultiplier;

    private void Start()
    {
        cameraPos = GetComponentInChildren<Camera>().transform;
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
            pause = !pause;

        if (pause)
            return;

        if (Input.GetKey(KeyCode.LeftShift))
            cameraMovementMultiplier = 2f;
        else
            cameraMovementMultiplier = 1f;

        Scroll();
        MoveCamera();
    }

    private void Scroll()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel");

        Vector3 targetLocation = followPoint.transform.position;
        Vector3 targetRotation = cameraPos.rotation.eulerAngles;

        targetLocation.y -= (scroll * 10) * ((maxY - minY) / scrollSpeed);
        targetLocation.y = Mathf.Clamp(targetLocation.y, minY, maxY);

        followPoint.transform.position = targetLocation;

        targetRotation.x = minXCameraRotation + ((targetLocation.y / maxY) * (maxXCameraRotation - minXCameraRotation));
        targetRotation.x = Mathf.Clamp(targetRotation.x, minXCameraRotation, maxXCameraRotation);

        cameraPos.rotation = Quaternion.Lerp(cameraPos.rotation, Quaternion.Euler(targetRotation), Time.deltaTime * turnSpeed);
    }

    private void MoveCamera()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        if (vertical > 0 || Input.mousePosition.y >= Screen.height - panBorderSize)
            if (followPoint.transform.position.z <= maxZ)
                followPoint.transform.Translate(Vector3.forward * panSpeed * cameraMovementMultiplier * Time.deltaTime, Space.World);

        if (vertical < 0 || Input.mousePosition.y <= panBorderSize)
            if (followPoint.transform.position.z >= minZ)
                followPoint.transform.Translate(Vector3.back * panSpeed * cameraMovementMultiplier * Time.deltaTime, Space.World);

        if (horizontal > 0 || Input.mousePosition.x >= Screen.width - panBorderSize)
            if (followPoint.transform.position.x <= maxX)
                followPoint.transform.Translate(Vector3.right * panSpeed * cameraMovementMultiplier * Time.deltaTime, Space.World);

        if (horizontal < 0 || Input.mousePosition.x <= panBorderSize)
            if (followPoint.transform.position.x >= minX)
                followPoint.transform.Translate(Vector3.left * panSpeed * cameraMovementMultiplier * Time.deltaTime, Space.World);

        cameraPos.position = Vector3.Lerp(cameraPos.position, followPoint.position, Time.deltaTime * followSpeed);
    }
}
