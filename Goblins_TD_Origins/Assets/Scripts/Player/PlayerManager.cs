using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    [HideInInspector]
    private Color blockedColor;
    [HideInInspector]
    public int HeartAmount;
    [HideInInspector]
    public int MoneyAmount;
    [HideInInspector]
    public int CurrentWave;
    [HideInInspector]
    public int MaxWave;

    [SerializeField] private int startMoneyAmount = 500;

    public static PlayerManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        blockedColor = new Color(255, 255, 255, 0.5f);
        MoneyAmount = startMoneyAmount;
    }

    private void Update()
    {
        CheckShopTowers();
        CheckUpgrades();
    }

    private void CheckShopTowers()
    {
        TowerBase[] towerBases = Shop.Instance.GetTowerBases();
        Image[] towerImages = TowerListHandler.Instance.AllTowers;

        for (int i = 0; i < towerBases.Length; i++)
        {
            if (towerBases[i].TowerCost > MoneyAmount)
            {
                towerImages[i].color = blockedColor;
            }
            else if (towerImages[i].color == blockedColor)
            {
                towerImages[i].color = Color.white;
            }
        }
    }

    private void CheckUpgrades()
    {
        foreach (GameObject upgradeImage in UpgradeHandler.Instance.GetUpgradeImages())
        {
            if (upgradeImage.GetComponent<UpgradeHud>().UpgradeCost > MoneyAmount)
            {
                upgradeImage.GetComponent<UpgradeInteractionHandler>().ToggleDisabledOverlay(true);
            }
            else
            {
                upgradeImage.GetComponent<UpgradeInteractionHandler>().ToggleDisabledOverlay(false);
            }
        }
    }
}
