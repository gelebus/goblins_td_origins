using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHud : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI heartsText;
    [SerializeField] private TextMeshProUGUI moneyText;
    [SerializeField] private TextMeshProUGUI wavesText;

    [Header("Money Animation")]
    [SerializeField] private float moneyAnimIncreaseRate = 250f;

    [Header("House Animation")]
    [SerializeField] private GameObject houseEmpty;

    [Header("Damage indication sound")]
    [SerializeField] private SoundBase screamSound;

    private float currentMoneyAmount;
    private float currentMoneyAnimIncrease;
    private GameObject[] houses;
    private int lastHeartAmount;
    private PlayerManager Player;

    private void Start()
    {
        Collider[] houseColliders = houseEmpty.GetComponentsInChildren<Collider>();
        houses = new GameObject[houseColliders.Length];
        for (int i = 0; i < houseColliders.Length; i++)
        {
            houses[i] = houseColliders[i].gameObject;
            houses[i].GetComponent<Renderer>().material.color = Color.white;
        }
        Player = PlayerManager.Instance;
        Player.HeartAmount = houses.Length;
        lastHeartAmount = Player.HeartAmount;

        StartCoroutine(DelayedStart());
    }

    private IEnumerator DelayedStart()
    {
        yield return new WaitForSeconds(.001f);
        currentMoneyAmount = Player.MoneyAmount;
        moneyText.text = currentMoneyAmount.ToString();
    }

    void Update()
    {
        UpdateHud();
    }

    void UpdateHud()
    {
        heartsText.text = Player.HeartAmount.ToString();
        wavesText.text = $"{Player.CurrentWave}/{Player.MaxWave}";
        if (currentMoneyAmount != Player.MoneyAmount)
        {
            UpdateMoneyHud();
        }
        if (lastHeartAmount != Player.HeartAmount || Player.HeartAmount == 0)
        {
            PlayHousingAnimation();
            AudioManager.Instance.PlaySound(houses[Player.HeartAmount], screamSound.SoundName);
        }
    }

    void UpdateMoneyHud()
    {
        currentMoneyAnimIncrease = moneyAnimIncreaseRate;

        if (currentMoneyAmount > Player.MoneyAmount)
        {
            if ((currentMoneyAmount - Player.MoneyAmount) / 100 >= 1)
            {
                currentMoneyAnimIncrease = currentMoneyAnimIncrease * 1.5f;
            }
            float moneyIncrement = Time.deltaTime * currentMoneyAnimIncrease;
            currentMoneyAmount -= moneyIncrement;

            if (currentMoneyAmount < Player.MoneyAmount)
                currentMoneyAmount = Player.MoneyAmount;
        }
        else
        {
            if ((Player.MoneyAmount - currentMoneyAmount) / 100 >= 1)
            {
                currentMoneyAnimIncrease = currentMoneyAnimIncrease * 1.5f;
            }
            float moneyIncrement = Time.deltaTime * currentMoneyAnimIncrease;
            currentMoneyAmount += moneyIncrement;

            if (currentMoneyAmount > Player.MoneyAmount)
                currentMoneyAmount = Player.MoneyAmount;
        }
        moneyText.text = Mathf.Ceil(currentMoneyAmount).ToString();
    }

    private void PlayHousingAnimation()
    {
        for (int i = Player.HeartAmount; i < lastHeartAmount; i++)
        {
            GameObject psObject = houses[i].transform.GetChild(0).gameObject;
            psObject.SetActive(true);
            houses[i].GetComponent<Renderer>().material.color = new Color(85f / 255f, 85f / 255f, 85f / 255f);
        }
        lastHeartAmount = Player.HeartAmount;
    }
}
