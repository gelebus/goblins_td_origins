using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseTower : MonoBehaviour
{
    [SerializeField] Transform FirePoint;

    private TowerStats towerStats;
    private float fireCountdown = 0.2f;

    private void Awake()
    {
        towerStats = GetComponent<TowerStats>();
    }

    void Update()
    {
        if (fireCountdown <= 0)
        {
            ShootPulse();
            fireCountdown = 1f / towerStats.FireRate;
        }
        fireCountdown -= Time.deltaTime;
    }

    void ShootPulse()
    {
        AudioManager.Instance.PlaySound(FirePoint.gameObject, towerStats.TowerInfo.FireSoundName);

        GameObject pulse = ObjectPooler.Instance.GetPoolObject(towerStats.GetAmmoInfo().PrefabKey);
        pulse.transform.position = FirePoint.position;
        pulse.transform.rotation = FirePoint.rotation;

        StartCoroutine(ReturnObj(pulse, towerStats.GetAmmoInfo().PrefabKey));

        Collider[] enemyCols = Physics.OverlapSphere(transform.position, towerStats.Range, LayerMask.GetMask("Enemy"));
        if (enemyCols == null || enemyCols.Length == 0)
        {
            return;
        }

        foreach(Collider col in enemyCols)
        {
            col.gameObject.GetComponent<Enemy>().TakeDamage(towerStats.AmmoDamage);
            col.gameObject.GetComponent<Enemy>().Slow(towerStats.GetSlowMultiplier(),towerStats.GetCanTargetCamo() , towerStats.GetCanSlowHeavy(), towerStats.GetCanSlowBoss());
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (towerStats == null)
        {
            return;
        }
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, towerStats.Range);
    }

    IEnumerator ReturnObj(GameObject obj, string key)
    {
        yield return new WaitForSeconds(2f);
        ObjectPooler.Instance.ReturnPoolObject(key, obj);
    }
}
