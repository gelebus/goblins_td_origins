using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TowerInfo
{
    [Header("spawning")]
    [SerializeField] string towerKey;
    [Header("UI")]
    [SerializeField] string towerName;
    [SerializeField] TowerUI towerUI;
    [Header("sound")]
    [SerializeField] string fireSoundName;

    public string TowerKey 
    { 
        get { return towerKey; } 
    }
    public string TowerName 
    {
        get { return towerName; } 
    }
    public string FireSoundName
    {
        get { return fireSoundName; }
    }
    public TowerUI TowerUI
    {
        get { return towerUI; }
    }
}
