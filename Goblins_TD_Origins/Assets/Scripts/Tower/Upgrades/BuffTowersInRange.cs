using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffTowersInRange : MonoBehaviour
{
    [SerializeField] private LayerMask towerLayer;
    [SerializeField] private float timeBetweenChecks;
    private float time;
    private bool enableBuff;

    private List<Collider> storedColliders;

    private void Start()
    {
        time = timeBetweenChecks;
        storedColliders = new List<Collider>();
    }

    private void Update()
    {
        if (!enableBuff)
            return;

        if (time <= 0)
        {
            AddBuffToSurroundingTowers();
            time = timeBetweenChecks;
        }

        time -= Time.deltaTime;
    }

    private void AddBuffToSurroundingTowers()
    {
        Collider[] towerColliders = Physics.OverlapSphere(transform.position, GetComponent<TowerStats>().Range, towerLayer);
        RemoveInactiveCollidersFromList();
        foreach (Collider tower in towerColliders)
        {
            if (tower.gameObject != gameObject && !DoesTowerHaveBuff(tower))
            {
                tower.GetComponent<TowerStats>().ReceiveBuffAmount.Add(GetComponent<TowerStats>().SendBuffAmount);
                storedColliders.Add(tower);
            }
        }
    }

    public void EnableBuff()
    {
        enableBuff = true;
    }

    public void DisableBuff()
    {
        enableBuff = false;
        Collider[] towerColliders = Physics.OverlapSphere(transform.position, GetComponent<TowerStats>().Range, towerLayer);
        foreach(Collider tower in towerColliders)
        {
            if (tower.gameObject != gameObject && DoesTowerHaveBuff(tower))
            {
                tower.GetComponent<TowerStats>().ReceiveBuffAmount.Remove(GetComponent<TowerStats>().SendBuffAmount);
            }
        }
        storedColliders = new List<Collider>();
    }

    private bool DoesTowerHaveBuff(Collider col)
    {
        for (int i = 0; i < storedColliders.Count; i++)
        {
            if(col == storedColliders[i])
            {
                return true;
            }
        }
        return false;
    }

    private void RemoveInactiveCollidersFromList()
    {
        for (int i = 0; i < storedColliders.Count; i++)
        {
            if (!storedColliders[i].gameObject.activeSelf)
            {
                storedColliders.Remove(storedColliders[i]);
                i = -1;
            }
        }
    }
}
