using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tower", menuName = "Tower/Create a new Tower Upgrade")]
public class UpgradeTowerBase : ScriptableObject
{
    [Header("UI")]
    [SerializeField] Sprite upgradeImage;
    [SerializeField] string upgradeTitle;
    [SerializeField] string upgradeInfo;
    [Header("Tower changes")]
    [SerializeField] int upgradeCost;
    [SerializeField] float range;
    [SerializeField] float fireRate;
    [SerializeField] bool canTargetCamo;
    [SerializeField] int sellValue;
    [SerializeField] float fireAnimationMultiplier;
    [Header("Ammo changes")]
    [SerializeField] AmmoInfo ammoInfo;
    [SerializeField] int damage;
    [Header("Optional")]
    [SerializeField] float explosionRadius;
    [SerializeField] bool ricochet;
    [SerializeField] int shootAmount;
    [Range(0f, 1f)]
    [SerializeField] float slowMultiplier;
    [SerializeField] bool canSlowHeavy;
    [SerializeField] bool canSlowBoss;
    [SerializeField] DotEffect dotEffect;
    [SerializeField] TowerBuff buffAmountTowersInRange;

    public Sprite UpgradeImage { get { return upgradeImage; } }
    public string UpgradeTitle { get { return upgradeTitle; } }
    public string UpgradeInfo { get { return upgradeInfo; } }
    public bool Ricochet
    { get { return ricochet; } }
    public float ShootAmount
    { get { return shootAmount; } }
    public float Range { get { return range; } }
    public int Damage { get { return damage; } }
    public float ExplosionRadius { get { return explosionRadius; } }
    public float FireRate { get { return fireRate; } }
    public float SlowMultiplier { get { return slowMultiplier; } }
    public bool CanTargetCamo { get { return canTargetCamo; } }
    public int UpgradeCost { get { return upgradeCost; } }
    public int SellValue { get { return sellValue; } }
    public float FireAnimationMultiplier { get { return fireAnimationMultiplier; } }
    public AmmoInfo AmmoInfo { get { return ammoInfo; } }
    public DotEffect DotEffect { get { return dotEffect; } }
    public TowerBuff BuffAmountTowersInRange { get { return buffAmountTowersInRange; } }
    public bool CanSlowHeavy { get { return canSlowHeavy; } }
    public bool CanSlowBoss { get { return canSlowBoss; } }
}
