using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UpgradePath 
{
    [SerializeField] private UpgradeTowerBase[] upgrades;
    public UpgradeTowerBase[] Upgrades 
    { 
        get { return upgrades; } 
    }
}
