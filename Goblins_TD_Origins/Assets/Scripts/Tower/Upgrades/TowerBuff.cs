using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TowerBuff
{
    [SerializeField] private StatNames statName;
    [SerializeField] private float amount;

    public StatNames StatName { get { return statName; } }

    public float Amount { get { return amount; } }
}
