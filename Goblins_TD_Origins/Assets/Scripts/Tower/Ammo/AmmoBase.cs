using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Ammo", menuName = "Tower/Create a new type of ammo")]
public class AmmoBase : ScriptableObject
{
    [SerializeField] AmmoInfo ammoInfo;
    [SerializeField] int baseDamage;
    [SerializeField] float speed;

    [Header("AOE damage (Optional)")]
    [SerializeField] float explosionRadius = 0f;

    [Header("slow multiplier (Optional)")]
    [Range(0.01f, 1f)]
    [SerializeField] float slowMultiplier = 1f;

    
    public AmmoInfo AmmoInfo
    {
        get { return ammoInfo; }
    }
    public int BaseDamage
    {
        get { return baseDamage; }
    }
    public float ExplosionRadius
    {
        get { return explosionRadius; }
    }
    public float Speed
    {
        get { return speed; }
    }
    public float SlowMultiplier
    {
        get { return slowMultiplier; }
    }
}
