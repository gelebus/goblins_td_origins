using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAmmoAfterAnim : MonoBehaviour
{
    [SerializeField] private ShootTower tower;
    private Transform currentTarget;
    private Transform firePoint;
    private TowerStats towerStats;
    void SpawnAmmo()
    {
        if (tower.GetTarget() == null)
        {
            return;
        }
        Transform target = currentTarget;
        firePoint = tower.GetFirePoint();
        towerStats = tower.GetComponent<TowerStats>();

        AudioManager.Instance.PlaySound(firePoint.gameObject, towerStats.TowerInfo.FireSoundName);

        if (towerStats.ShootAmount > 0)
        {
            ShootMultipleAmmoObj();
            return;
        }

        GameObject bullet = ObjectPooler.Instance.GetPoolObject(towerStats.GetAmmoInfo().PrefabKey);
        bullet.transform.position = firePoint.position;
        bullet.transform.rotation = firePoint.rotation;
        Ammo ammo = bullet.GetComponent<Ammo>();
        
        if (ammo)
        {
            ammo.Init(towerStats);
            ammo.Seek(target);
        }
    }

    private void ShootMultipleAmmoObj()
    {
        Transform target = currentTarget;
        List<GameObject> bullets = new List<GameObject>();
        bool above = false;
        float yOffset = 0f;

        for (int i = 0; i < towerStats.ShootAmount + 1; i++)
        {
            GameObject bullet = ObjectPooler.Instance.GetPoolObject(towerStats.GetAmmoInfo().PrefabKey);
            bullets.Add(bullet);
        }

        foreach (GameObject bullet in bullets)
        {
            Vector3 _firepoint;
            if (above)
            {
                yOffset += 0.2f;
                _firepoint = new Vector3(firePoint.position.x, firePoint.position.y + yOffset, firePoint.position.z);
            }
            else
            {
                _firepoint = new Vector3(firePoint.position.x, firePoint.position.y - yOffset, firePoint.position.z);
            }
            bullet.transform.position = _firepoint;
            bullet.transform.rotation = firePoint.rotation;
            Ammo ammo = bullet.GetComponent<Ammo>();

            if (ammo != null)
            {
                ammo.Init(towerStats);
                ammo.Seek(target);
            }
            above = !above;
        }
    }

    private void Update()
    {
        if(tower.GetTarget() == null)
        {
            return;
        }
        currentTarget = tower.GetTarget();
    }
}
