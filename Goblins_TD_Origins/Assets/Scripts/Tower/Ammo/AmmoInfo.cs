using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AmmoInfo
{
    [SerializeField] string prefabKey;
    [SerializeField] string impactEffectKey;
    [SerializeField] string impactSoundName;

    public string PrefabKey
    {
        get { return prefabKey; }
    }
    public string ImpactEffectKey
    {
        get { return impactEffectKey; }
    }
    public string ImpactSoundName
    {
        get { return impactSoundName; }
    }
}
