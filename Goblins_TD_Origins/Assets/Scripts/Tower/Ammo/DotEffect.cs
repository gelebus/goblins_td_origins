using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DotEffect
{
    [SerializeField] private string dotEffectKey;
    [SerializeField] private float damageTotal;
    [SerializeField] private float executionTime;
    [SerializeField] private DotEffectName dotEffectName;

    public string DotEffectKey
    {
        get { return dotEffectKey; }
    }
    public float DamageTotal
    {
        get { return damageTotal; }
    }
    public float ExecutionTime
    {
        get { return executionTime; }
    }
    public DotEffectName DotEffectName
    {
        get { return dotEffectName; }
    }
}

public enum DotEffectName
{
    None,
    Poison,
    Burn
}
