using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    private Transform target;
    private AmmoInfo ammoInfo;
    private bool hitting;
    private int damage;
    private float explosionRadius;
    private float speed;
    private DotEffect dotEffect;
    private bool ricochet;

    public void Init(TowerStats towerStats)
    {
        ammoInfo = towerStats.GetAmmoInfo();
        damage = towerStats.AmmoDamage;
        explosionRadius = towerStats.AmmoExplosionRadius;
        speed = towerStats.AmmoSpeed;
        dotEffect = towerStats.GetDotEffect();
        ricochet = towerStats.HasRicochet();
    }
    public void Seek(Transform _target)
    {
        target = _target;
    }

    private void Update()
    {
        if(target == null)
        {
            ObjectPooler.Instance.ReturnPoolObject(ammoInfo.PrefabKey, gameObject);
            return;
        }
        Move();
    }

    private void Move()
    {
        Vector3 direction = target.position - transform.position;
        float distanceThisFrame = Time.deltaTime * speed;

        if(direction.magnitude <= distanceThisFrame)
        {
            if (!hitting)
            {
                if (explosionRadius > 0)
                {
                    DamageAoe();
                }
                else
                {
                    DamageTarget(target);
                }
                StartCoroutine(HitTarget());
            }
            return;
        }

        transform.Translate(direction.normalized * distanceThisFrame, Space.World);
        Rotate();
    }

    private void Rotate()
    {
        var lookPos = target.position - transform.position;
        var rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 20);
    }

    private void DamageAoe()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, LayerMask.GetMask("Enemy"));
        if(colliders == null || colliders.Length == 0) 
        {
            return;
        }

        foreach(Collider col in colliders)
        {
            if (col.gameObject.GetComponent<Enemy>())
            {
                DamageTarget(col.transform);
            }
        }
    }

    private IEnumerator HitTarget()
    {
        hitting = true;
        GameObject impactEffect = ObjectPooler.Instance.GetPoolObject(ammoInfo.ImpactEffectKey);
        impactEffect.transform.position = transform.position;
        impactEffect.transform.rotation = transform.rotation;

        AudioManager.Instance.PlaySound(impactEffect, ammoInfo.ImpactSoundName);
        if (ricochet)
        {
            List<GameObject> enemies = new List<GameObject>();
            for (int i = 0; i < SpawnManager.Instance.AllActiveEnemies.Count; i++)
            {
                if (SpawnManager.Instance.AllActiveEnemies[i] != target.gameObject)
                {
                    enemies.Add(SpawnManager.Instance.AllActiveEnemies[i]);
                }
            }
            if(enemies.Count > 0)
            {
                speed = 20;
                target = enemies[Random.Range(0, enemies.Count)].transform;
            }
        }
        else
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        if (ricochet)
        {
            yield return new WaitForSeconds(0.5f);
            hitting = false;
            ricochet = false;
        }
        yield return new WaitForSeconds(2f);
        ObjectPooler.Instance.ReturnPoolObject(ammoInfo.ImpactEffectKey, impactEffect);
        if (!ricochet && hitting)
        {
            ObjectPooler.Instance.ReturnPoolObject(ammoInfo.PrefabKey, gameObject);
            hitting = false;
        }
        ricochet = false;
    }
    private void DamageTarget(Transform enemy)
    {
        enemy.GetComponent<Enemy>().TakeDamage(damage);
        if(dotEffect != null && dotEffect.DotEffectName != DotEffectName.None)
        {
            enemy.GetComponent<Enemy>().AddDotEffect(dotEffect);
        }
    }
}
