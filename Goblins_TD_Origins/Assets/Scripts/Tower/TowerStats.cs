using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerStats : MonoBehaviour
{
    [SerializeField] private TowerBase TowerBase;
    [HideInInspector]
    public List<TowerBuff> ReceiveBuffAmount;

    public TowerBuff SendBuffAmount { get { return GetSendBuffAmount(); } }

    private TowerBuff GetSendBuffAmount()
    {
        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if (UpgradeIndex[i] - 1 >= 0 && TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].BuffAmountTowersInRange.StatName != StatNames.None)
            {
                return TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].BuffAmountTowersInRange;
            }
        }
        return new TowerBuff();
    }

    public int[] UpgradeIndex { get; private set; }

    private void Awake()
    {
        UpgradeIndex = new int[3];
        ReceiveBuffAmount = new List<TowerBuff>();
    }

    //tower
    public float Range { get { return TowerBase.Range + GetStat(StatNames.Range); } }

    public float FireRate { get { return TowerBase.FireRate + GetStat(StatNames.FireRate); } }

    public int SellValue { get { return TowerBase.SellValue + (int)GetStat(StatNames.SellValue); } }

    public float FireAnimationMultiplier { get { return TowerBase.FireAnimationMultiplier + GetStat(StatNames.FireAnimationMultiplier); } }
    
    public TowerInfo TowerInfo { get { return TowerBase.TowerInfo; } }

    public bool GetCanTargetCamo()
    {
        bool canTargetCamo = TowerBase.CanTargetCamo;

        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if (HasAndCanUpgrade(i))
            {
                if (TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].CanTargetCamo)
                {
                    canTargetCamo = true;
                }
            }
        }

        return canTargetCamo;
    }

    //ammo
    public int AmmoDamage { get { return TowerBase.Ammo.BaseDamage + (int)GetStat(StatNames.Damage); } }

    public float AmmoExplosionRadius { get { return TowerBase.Ammo.ExplosionRadius + GetStat(StatNames.ExplosionRadius); } }

    public float AmmoSpeed { get { return TowerBase.Ammo.Speed; } }
    public bool HasRicochet()
    {
        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if (UpgradeIndex[i] - 1 >= 0 && TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].Ricochet)
            {
                return true;
            }
        }
        return false;
    }
    public float ShootAmount { get { return GetStat(StatNames.ShootAmount); } }
    public float GetSlowMultiplier() 
    {
        float multiplier = TowerBase.Ammo.SlowMultiplier;

        if(GetStat(StatNames.SlowMultiplier) > 0)
        {
            return GetStat(StatNames.SlowMultiplier);
        }

        return multiplier;
    }
    public bool GetCanSlowHeavy()
    {
        bool canSlowHeavy = TowerBase.CanSlowHeavy;

        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if (HasAndCanUpgrade(i))
            {
                if (TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].CanSlowHeavy)
                {
                    canSlowHeavy = true;
                }
            }
        }

        return canSlowHeavy;
    }
    public bool GetCanSlowBoss()
    {
        bool canSlowBoss = TowerBase.CanSlowBoss;

        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if (HasAndCanUpgrade(i))
            {
                if (TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].CanSlowBoss)
                {
                    canSlowBoss = true;
                }
            }
        }

        return canSlowBoss;
    }
    public AmmoInfo GetAmmoInfo()
    {
        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if (UpgradeIndex[i] - 1 >= 0 && TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].AmmoInfo.ImpactEffectKey != "" && TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].AmmoInfo.PrefabKey != "" && TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].AmmoInfo.ImpactSoundName != "")
            {
                return TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].AmmoInfo;
            }
        }
        
        return TowerBase.Ammo.AmmoInfo;
    }

    public DotEffect GetDotEffect()
    {
        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if (UpgradeIndex[i] - 1 >= 0 && TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].DotEffect.DotEffectName != DotEffectName.None)
            {
                return TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].DotEffect;
            }
        }

        return null;
    }

    //upgrades
    public UpgradePath[] UpgradePaths
    {
        get { return TowerBase.UpgradePaths; }
    }
    public UpgradeTowerBase GetCurrentUpgrade(int pathIndex)
    {
        return TowerBase.UpgradePaths[pathIndex].Upgrades[UpgradeIndex[pathIndex]];
    }

    public void SetUpgrade(int pathIndex, int upgradeIndex)
    {
        UpgradeIndex[pathIndex] = upgradeIndex;
    }
    private bool HasAndCanUpgrade(int pathIndex)
    {
        return UpgradeIndex[pathIndex] < TowerBase.UpgradePaths[pathIndex].Upgrades.Length && UpgradeIndex[pathIndex] > 0;
    }

    public bool IsOnLastUpgrade(int pathIndex)
    {
        return UpgradeIndex[pathIndex] == TowerBase.UpgradePaths[pathIndex].Upgrades.Length - 1;
    }

    public bool IsPathUpgradeable(int pathIndex)
    {
        int amountOfActivePaths = 0;
        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if(i != pathIndex && UpgradeIndex[i] > 0)
            {
                amountOfActivePaths++;
            }
        }
        return amountOfActivePaths < 2;
    }

    public float GetActivePathsAmount()
    {
        int amountOfActivePaths = 0;
        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if (UpgradeIndex[i] > 0)
            {
                amountOfActivePaths++;
            }
        }
        return amountOfActivePaths;
    }

    public bool IsMaxPathUpgradeAllowed()
    {
        bool maxUpgradeAllowed = true;
        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if(UpgradeIndex[i] == TowerBase.UpgradePaths[i].Upgrades.Length - 1)
            {
                maxUpgradeAllowed = false;
            }
        }
        return maxUpgradeAllowed;
    }

    private float GetStat(StatNames statName)
    {
        float returnValue = 0f;

        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if (statName == StatNames.Damage && HasAndCanUpgrade(i))
            {
                returnValue += TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].Damage;
            }
            else if (statName == StatNames.ExplosionRadius && HasAndCanUpgrade(i))
            {
                returnValue += TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].ExplosionRadius;
            }
            else if (statName == StatNames.FireAnimationMultiplier && HasAndCanUpgrade(i))
            {
                returnValue += TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].FireAnimationMultiplier;
            }
            else if (statName == StatNames.FireRate && HasAndCanUpgrade(i))
            {
                returnValue += TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].FireRate;
            }
            else if (statName == StatNames.Range && HasAndCanUpgrade(i))
            {
                returnValue += TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].Range;
            }
            else if (statName == StatNames.SellValue && HasAndCanUpgrade(i))
            {
                returnValue += TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].SellValue;
            }else if(statName == StatNames.ShootAmount && HasAndCanUpgrade(i))
            {
                returnValue += TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].ShootAmount;
            }else if(statName == StatNames.SlowMultiplier && HasAndCanUpgrade(i))
            {
                returnValue += TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].SlowMultiplier;
            }
        }

        CheckForBuffActivation();

        returnValue += GetBuffAmount(statName);

        return returnValue;
    }

    private void CheckForBuffActivation()
    {
        for (int i = 0; i < TowerBase.UpgradePaths.Length; i++)
        {
            if (HasAndCanUpgrade(i) && TowerBase.UpgradePaths[i].Upgrades[UpgradeIndex[i] - 1].BuffAmountTowersInRange.StatName != StatNames.None)
            {
                GetComponent<BuffTowersInRange>().EnableBuff();
            }
        }
    }

    private float GetBuffAmount(StatNames statName)
    {
        float amount = 0;

        for (int i = 0; i < ReceiveBuffAmount.Count; i++)
        {
            if(ReceiveBuffAmount[i].StatName == statName)
            {
                amount += ReceiveBuffAmount[i].Amount;
            }
        }
        return amount;
    }
}



public enum StatNames
{
    None,
    Damage,
    ExplosionRadius,
    FireAnimationMultiplier,
    FireRate,
    Range,
    SellValue,
    ShootAmount,
    SlowMultiplier
}
