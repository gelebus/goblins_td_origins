using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootTower : MonoBehaviour
{
    private TowerStats towerStats;

    [SerializeField] Transform FirePoint;
    private float fireCountdown = 0.2f;
    private Transform target;
    private GameObject nearestEnemy = null;
    
    [Header("Turning left/right")]
    public Transform RotatePointY;
    public float RotateSpeedY = 7f;

    [Header("Turning up/down (optional)")]
    public Transform RotatePointX;
    public float MaxXRotation = 40f;
    public float MinXRotation = 8f;
    public float RotateSpeedX = 7f;
    public float XRotationOffset = 0f;

    [Header("Animation (optional)")]
    [SerializeField] private Transform animObj;

    private bool hasAnimator;

    private void Awake()
    {
        towerStats = GetComponent<TowerStats>();
    }
    private void Start()
    {
        if (animObj)
        {
            hasAnimator = animObj.GetComponent<Animator>();
        }
        InvokeRepeating("UpdateTarget", 0f, 0.05f);
    }

    private void UpdateTarget()
    {
        if (target == null)
        {
            List<GameObject> enemies = FindEnemiesDetected();
            float shortestDistance = Mathf.Infinity;
            foreach (GameObject enemy in enemies)
            {
                float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                if (distanceToEnemy < shortestDistance)
                {
                    shortestDistance = distanceToEnemy;
                    nearestEnemy = enemy;
                }
            }
        }
        
        if(nearestEnemy != null && Vector3.Distance(transform.position, nearestEnemy.transform.position) < towerStats.Range)
        {
            if (nearestEnemy.GetComponent<Enemy>().IsDead)
            {
                target = null;
            }
            else
            {
                target = nearestEnemy.transform;
            }
        }
        else
        {
            target = null;
        }
    }

    private List<GameObject> FindEnemiesDetected()
    {
        if (towerStats.GetCanTargetCamo())
        {
            return SpawnManager.Instance.AllActiveEnemies;
        }
        else
        {
            return SpawnManager.Instance.ActiveNormalEnemies;
        }
    }

    private void Update()
    {
        if (target == null)
        {
            return;
        }

        Rotate();

        if(fireCountdown <= 0)
        {
            if (hasAnimator)
            {
                StartCoroutine(PlayShootAnim());
            }
            else
            {
                Shoot();
            }
            fireCountdown = 1f / towerStats.FireRate;
        }
        fireCountdown -= Time.deltaTime;
    }
    private void Shoot()
    {
        AudioManager.Instance.PlaySound(FirePoint.gameObject, towerStats.TowerInfo.FireSoundName);

        if(towerStats.ShootAmount > 0)
        {
            ShootMultipleAmmoObj();
            return;
        }
        GameObject bullet = ObjectPooler.Instance.GetPoolObject(towerStats.GetAmmoInfo().PrefabKey);
        bullet.transform.position = FirePoint.position;
        bullet.transform.rotation = FirePoint.rotation;
        Ammo ammo = bullet.GetComponent<Ammo>();
        
        if(ammo != null)
        {
            ammo.Init(towerStats);
            ammo.Seek(target);
        }
    }

    private void ShootMultipleAmmoObj()
    {
        List<GameObject> bullets = new List<GameObject>();
        bool above = false;
        float yOffset = 0f;

        for (int i = 0; i < towerStats.ShootAmount + 1; i++)
        {
            GameObject bullet = ObjectPooler.Instance.GetPoolObject(towerStats.GetAmmoInfo().PrefabKey);
            bullets.Add(bullet);
        }

        foreach (GameObject bullet in bullets)
        {
            Vector3 _firepoint;
            if (above)
            {
                yOffset += 0.2f;
                _firepoint = new Vector3(FirePoint.position.x, FirePoint.position.y + yOffset, FirePoint.position.z);
            }
            else
            {
                _firepoint = new Vector3(FirePoint.position.x, FirePoint.position.y - yOffset, FirePoint.position.z);
            }
            bullet.transform.position = _firepoint;
            bullet.transform.rotation = FirePoint.rotation;
            Ammo ammo = bullet.GetComponent<Ammo>();

            if (ammo != null)
            {
                ammo.Init(towerStats);
                ammo.Seek(target);
            }
            above = !above;
        }
    }

    private IEnumerator PlayShootAnim()
    {
        Animator anim = animObj.GetComponent<Animator>();
        if (anim)
        {
            anim.SetBool("Fire", true);
            anim.SetFloat("SpeedMultiplier", towerStats.FireAnimationMultiplier);
            yield return new WaitForSeconds(0.5f);            
            anim.SetBool("Fire", false);
        }
    }
    private void Rotate()
    {
        Vector3 direction = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(RotatePointY.rotation, lookRotation, Time.deltaTime * RotateSpeedY).eulerAngles;
        RotatePointY.rotation = Quaternion.Euler(new Vector3(0f, rotation.y, 0f));
        
        if (RotatePointX != null)
        {
            Quaternion xRotation = Quaternion.Euler(new Vector3(CalcXRotationValue(), rotation.y, 0f));
            Vector3 rotationX = Quaternion.Lerp(RotatePointX.rotation, xRotation, Time.deltaTime * RotateSpeedX).eulerAngles;
            RotatePointX.rotation = Quaternion.Euler(new Vector3(rotationX.x, rotationX.y, 0f));
        }
    }
    private float CalcXRotationValue()
    {
        if (target == null)
        {
            return 0f;
        }
        float enemyDistance = Vector3.Distance(transform.position, target.transform.position);
        float xRotationValue = -((enemyDistance / towerStats.Range) - 1) * MaxXRotation + XRotationOffset;
        xRotationValue = Mathf.Clamp(xRotationValue, MinXRotation, MaxXRotation);
        return xRotationValue;
    }
    private void OnDrawGizmosSelected()
    {
        if(towerStats == null)
        {
            return;
        }
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, towerStats.Range);
    }
    public Transform GetTarget()
    {
        return target;
    }
    public Transform GetFirePoint()
    {
        return FirePoint;
    }
}