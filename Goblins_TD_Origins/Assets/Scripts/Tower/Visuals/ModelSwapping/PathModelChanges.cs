using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PathModelChanges
{
    [SerializeField] private TowerModelChange[] upgradeChanges;

    public TowerModelChange[] UpgradeChanges
    {
        get { return upgradeChanges; }
    }
}
