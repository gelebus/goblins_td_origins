using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TowerModelChange
{
    [SerializeField] private GameObject[] activateables;
    [SerializeField] private GameObject[] deactivateables;

    public GameObject[] Activateables
    {
        get { return activateables; }
    }
    public GameObject[] Deactivateables
    {
        get { return deactivateables; }
    }
}
