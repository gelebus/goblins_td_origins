using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerModelHandler : MonoBehaviour
{
    [SerializeField] private PathModelChanges[] pathModelChanges;

    public void SwapModel(int pathIndex, int upgradeIndex)
    {
        GameObject[] activateables = pathModelChanges[pathIndex].UpgradeChanges[upgradeIndex].Activateables;
        GameObject[] deactivateables = pathModelChanges[pathIndex].UpgradeChanges[upgradeIndex].Deactivateables;

        if(activateables.Length > 0)
        {
            foreach(GameObject obj in activateables)
            {
                if (obj == null)
                {
                    return;
                }
                obj.SetActive(true);
            }
        }
        if(deactivateables.Length > 0)
        {
            foreach(GameObject obj in deactivateables)
            {
                if (obj == null)
                {
                    return;
                }
                obj.SetActive(false);
            }
        }
    }

    public void ResetModel()
    {
        foreach(PathModelChanges pathModelChange in pathModelChanges)
        {
            foreach(TowerModelChange towerModelChange in pathModelChange.UpgradeChanges)
            {
                foreach(GameObject obj in towerModelChange.Activateables)
                {
                    if(obj == null)
                    {
                        return;
                    }
                    obj.SetActive(false);
                }
            }
        }
        foreach(PathModelChanges pathModelChange in pathModelChanges)
        {
            foreach(GameObject obj in pathModelChange.UpgradeChanges[0].Deactivateables)
            {
                if (obj == null)
                {
                    return;
                }
                obj.SetActive(true);
            }
        }
    }
}
