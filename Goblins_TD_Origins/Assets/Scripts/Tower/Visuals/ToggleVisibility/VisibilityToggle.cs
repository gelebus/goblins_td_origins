using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibilityToggle : MonoBehaviour
{
    [SerializeField] Material seeThroughMaterial;

    private Renderer rend;

    private Material ownMaterial;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
        ownMaterial = rend.material;
    }

    public void TurnVisible()
    {
        rend.material = ownMaterial;
    }

    public void TurnInvisible()
    {
        rend.material = seeThroughMaterial;
    }
}
