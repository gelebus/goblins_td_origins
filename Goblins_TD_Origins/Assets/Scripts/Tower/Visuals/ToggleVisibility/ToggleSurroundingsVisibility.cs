using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleSurroundingsVisibility : MonoBehaviour
{
    Collider[] colliders;
 
    private void OnEnable()
    {
        StartCoroutine(ShowRadius());
    }

    private IEnumerator ShowRadius()
    {
        yield return null;
        TowerStats tower = GetComponentInParent<TowerStats>();
        colliders = Physics.OverlapSphere(transform.position, tower.Range, LayerMask.GetMask("Placeable", "Unplaceable"));
        foreach (Collider col in colliders)
        {
            if (col.gameObject.GetComponent<VisibilityToggle>() && gameObject.transform.position.y - 1.82 < col.gameObject.transform.position.y)
            {
                col.gameObject.GetComponent<VisibilityToggle>().TurnInvisible();
            }
        }
    }

    private void OnDisable()
    {
        if (colliders == null)
            return;

        foreach (Collider col in colliders)
        {
            if (col != null && col.gameObject.GetComponent<VisibilityToggle>())
            {
                col.gameObject.GetComponent<VisibilityToggle>().TurnVisible();
            }
        }
    }
}
