using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private Transform drawLocation;
    [SerializeField] private Transform stringLocation;

    private Transform startLocation;

    private void Start()
    {
        startLocation = transform;
    }

    private void Update()
    {
        if (anim.GetBool("Fire"))
        {
            stringLocation.position = drawLocation.position;
        }
        else
        {
            stringLocation.position = startLocation.position;
        }
    }
}
