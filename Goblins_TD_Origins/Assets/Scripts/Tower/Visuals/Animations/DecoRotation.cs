using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecoRotation : MonoBehaviour
{
    [SerializeField] float rotationSpeed = 70f;
    void Update()
    {
        Vector3 rotation = transform.rotation.eulerAngles;
        rotation.y += Time.deltaTime * rotationSpeed;
        transform.rotation = Quaternion.Euler(rotation);
    }
}
