using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "TowerUI", menuName = "Tower/Create a new TowerUI")]
public class TowerUI : ScriptableObject
{
    [SerializeField] Color original;
    [SerializeField] Color hover;
    [SerializeField] Color selected;

    public Color Original
    {
        get { return original; }
    }
    public Color Hover
    {
        get { return hover; }
    }
    public Color Selected
    {
        get { return selected; }
    }
}
