using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tower", menuName = "Tower/Create a new Tower")]
public class TowerBase : ScriptableObject
{
    [SerializeField] TowerInfo towerInfo;
    [Header("stats")]
    [SerializeField] float range;
    [SerializeField] float fireRate;
    [SerializeField] bool canTargetCamo;
    [Space(10)]
    [SerializeField] int towerCost;
    [SerializeField] int sellValue;
    [Header("animation (optional)")]
    [SerializeField] float fireAnimationMultiplier = 1;
    [Space(15)]
    [SerializeField] AmmoBase ammo;
    [Space(10)]
    [SerializeField] UpgradePath[] upgrades;
    [Header("slow (optional)")]
    [SerializeField] bool canSlowHeavy;
    [SerializeField] bool canSlowBoss;

    public TowerInfo TowerInfo
    {
        get { return towerInfo; }
    }
    public float Range
    {
        get { return range; }
    }
    public float FireRate
    {
        get { return fireRate; }
    }
    public bool CanTargetCamo
    {
        get { return canTargetCamo; }
    }
    public AmmoBase Ammo
    {
        get { return ammo; }
    }
    public int TowerCost
    {
        get { return towerCost; }
    }
    public int SellValue
    {
        get { return sellValue; }
    }
    public UpgradePath[] UpgradePaths
    {
        get { return upgrades; }
    }
    public float FireAnimationMultiplier
    {
        get { return fireAnimationMultiplier; }
    }
    public bool CanSlowHeavy 
    { 
        get { return canSlowHeavy; } 
    }
    public bool CanSlowBoss 
    { 
        get { return canSlowBoss; } 
    }
}
