using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{
    public void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1f;
    }

    public void PlayButtonMainMenu(GameObject mainMenuContainer)
    {
        Animator anim = mainMenuContainer.GetComponent<Animator>();
        anim.SetBool("PlayIsClicked", true);

        StartCoroutine(GoToPlayMenu());
    }

    private IEnumerator GoToPlayMenu()
    {
        yield return new WaitForSeconds(0.8f);
        SceneManager.LoadScene("PlayMenu");
    }

    public void BackButtonPlayMenu(GameObject playMenuContainer)
    {
        Animator anim = playMenuContainer.GetComponent<Animator>();
        anim.SetBool("BackIsClicked", true);

        StartCoroutine(GoToMainMenu());
    }

    private IEnumerator GoToMainMenu()
    {
        yield return new WaitForSeconds(0.3f);
        SceneManager.LoadScene("MainMenu");
    }

    public void ReturnToMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }
}