using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public List<Pool> Pools;
    public Dictionary<string, Queue<GameObject>> PoolDictionary;

    private Dictionary<string, GameObject> prefabDictionary;
    private Dictionary<string, Transform> parentDictionary;

    public static ObjectPooler Instance { get; private set; }

    private void Start()
    {
        if(Instance == null)
        {
            Instance = this;
        }

        PoolDictionary = new Dictionary<string, Queue<GameObject>>();
        prefabDictionary = new Dictionary<string, GameObject>();
        parentDictionary = new Dictionary<string, Transform>();

        foreach(Pool pool in Pools)
        {
            Queue<GameObject> poolObjects = new Queue<GameObject>();

            for (int i = 0; i < pool.Size; i++)
            {
                GameObject obj = Instantiate(pool.Prefab);
                obj.transform.parent = pool.PoolParent;
                obj.SetActive(false);
                poolObjects.Enqueue(obj);
            }

            PoolDictionary.Add(pool.Key, poolObjects);
            prefabDictionary.Add(pool.Key, pool.Prefab);
            parentDictionary.Add(pool.Key, pool.PoolParent);
        }
    }

    public GameObject GetPoolObject(string key)
    {
        GameObject poolObject;
        if(PoolDictionary[key].Count > 0)
        {
            poolObject = PoolDictionary[key].Dequeue();
            poolObject.SetActive(true);
        }
        else
        {
            poolObject = Instantiate(prefabDictionary[key]);
            poolObject.transform.parent = parentDictionary[key];
        }
        return poolObject;
    }

    public void ReturnPoolObject(string key, GameObject poolObject)
    {
        poolObject.transform.GetChild(0).gameObject.SetActive(true);
        PoolDictionary[key].Enqueue(poolObject);
        poolObject.SetActive(false);
    }
}
