using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenuUI;
    [SerializeField] private GameObject optionsMenuUI;
    [SerializeField] private GameObject audioMenuUI;
    [SerializeField] private GameObject gameOptionsMenuUI;
    [SerializeField] private GameObject startLevelUI;

    private bool isPaused = false;
    private bool pauseButtonPressed = false;

    private void Update()
    {
        if (!startLevelUI.activeSelf && Input.GetKeyDown(KeyCode.Escape))
        {
            pauseButtonPressed = true;
        }

        if (pauseButtonPressed)
            PauseMenuHandler();
    }

    private void PauseMenuHandler()
    {
        pauseButtonPressed = false;
        isPaused = !isPaused;

        pauseMenuUI.SetActive(isPaused);

        if (!isPaused)
        {
            optionsMenuUI.SetActive(isPaused);
            audioMenuUI.SetActive(isPaused);
            gameOptionsMenuUI.SetActive(isPaused);
        }

        if (isPaused)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

    public void ResumeButtonPress()
    {
        isPaused = false;

        pauseMenuUI.SetActive(isPaused);
        Time.timeScale = 1;
    }

    public void OptionsButtonPress()
    {
        optionsMenuUI.SetActive(true);
    }

}
