using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour
{
    [SerializeField] GameObject towerOptions;
    [SerializeField] Text towerName;
    [SerializeField] Text sellText;

    private TowerBase towerToBuild;
    private GameObject towerToUpgrade;
    private GameObject towerToSell;
    private Animator towerOptionsAnim;

    public static BuildManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        towerOptionsAnim = towerOptions.GetComponent<Animator>();
    }

    public void SetTowerToBuild(TowerBase tower)
    {
        towerToBuild = tower;

        if (towerOptionsAnim.isActiveAndEnabled)
        {
            if (towerToUpgrade)
            {
                towerToUpgrade.transform.GetChild(0).gameObject.SetActive(false);
            }
            towerToUpgrade = null;
            ActivateTowerOptionsFadeOut();
        }
    }

    public void SetTowerToUpgrade(GameObject tower, GameObject upgradeImage)
    {
        if (tower != null && towerToUpgrade != null)
        {
            towerToUpgrade.transform.GetChild(0).gameObject.SetActive(false);
        }
        if (tower != null)
        {
            towerToBuild = null;

            TowerStats towerStats = tower.GetComponent<TowerStats>();
            Shop.Instance.ResetTowerImages(true, null, towerStats.TowerInfo.TowerUI.Original);

            towerName.text = towerStats.TowerInfo.TowerName;
            sellText.text = towerStats.SellValue.ToString();

            tower.transform.GetChild(0).gameObject.SetActive(true);
            towerToSell = tower;
            towerToUpgrade = tower;
            if (towerOptions.activeSelf && upgradeImage != null)
            {
                UpgradeHandler.Instance.StartImageAnimation(upgradeImage);
            }
            else
            {
                UpgradeHandler.Instance.ShowUpgradeOptions(tower);
                ActivateTowerOptionsFadeIn();
            }
        }
        else if (towerToUpgrade != null)
        {
            towerToUpgrade.transform.GetChild(0).gameObject.SetActive(false);
            towerToUpgrade = null;
            ActivateTowerOptionsFadeOut();
        }
    }

    private void ActivateTowerOptionsFadeIn()
    {
        towerOptions.SetActive(true);
        towerOptionsAnim.SetBool("TowerSelected", true);

        if (GetComponent<DialogueTrigger>())
        {
            StartCoroutine(ShowDialogue());
        }
    }

    private IEnumerator ShowDialogue()
    {
        yield return new WaitForSeconds(0.3f);
        GetComponent<DialogueTrigger>().TriggerDialogue();
    }

    public void ActivateTowerOptionsFadeOut()
    {
        towerOptionsAnim.SetBool("TowerSelected", false);
        StartCoroutine(DisableTowerOptions());
    }

    private IEnumerator DisableTowerOptions()
    {
        yield return new WaitForSeconds(0.1f);
        towerOptions.SetActive(false);
    }

    public TowerBase GetTowerToBuild()
    {
        return towerToBuild;
    }

    public GameObject GetTowerToUpgrade()
    {
        return towerToUpgrade;
    }

    public GameObject GetTowerToSell()
    {
        towerToUpgrade = null;
        return towerToSell;
    }
}