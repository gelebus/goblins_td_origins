using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "Sound", menuName = "Sound/Create a new Sound")]
public class SoundBase : ScriptableObject
{
    [SerializeField] private string soundName;
    [SerializeField] private AudioClip audioClip;
    [SerializeField] private AudioMixerGroup audioMixerGroup;
    [Range(0f,1f)]
    [SerializeField] private float volume;
    [Range(0.1f, 3f)]
    [SerializeField] private float pitch;
    [Range(0f,1f)]
    [SerializeField] private float spatialBlend;
    [SerializeField] private bool loop;

    public string SoundName
    {
        get { return soundName; }
    }
    public AudioClip AudioClip
    {
        get { return audioClip; }
    }
    public AudioMixerGroup AudioMixerGroup
    {
        get { return audioMixerGroup; }
    }
    public float Volume
    {
        get { return volume; }
    }
    public float Pitch
    {
        get { return pitch; }
    }
    public float SpatialBlend
    {
        get { return spatialBlend; }
    }
    public bool Loop
    {
        get { return loop; }
    }
}
