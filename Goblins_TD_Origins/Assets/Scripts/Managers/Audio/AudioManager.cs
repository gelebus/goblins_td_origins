using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioMixer audioMixer;

    [SerializeField] private Theme mainTheme;
    [SerializeField] private Theme gameTheme;
    public SoundBase[] Sounds;

    public static AudioManager Instance { get; private set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        PlaySound(mainTheme.AudioSourceObj, mainTheme.ThemeBase.SoundName);
        mainTheme.ThemeBase.Snapshot.TransitionTo(0.1f);
        mainTheme.Playing = true;
    }

    private void OnLevelWasLoaded(int level)
    {
        if(level == 0 || level == 1)
        {
            StartCoroutine(TransitionWithDelay(mainTheme));
        }
        else if(level == 2 || level == 3)
        {
            StartCoroutine(TransitionWithDelay(gameTheme));
        }
    }

    public void PlaySound(GameObject target, string soundName)
    {
        //finding sound
        SoundBase sound = Array.Find(Sounds, sound => sound.SoundName == soundName);
        if(sound == null)
        {
            return;
        }

        //checking if there is an audiosource already
        AudioSource source = target.GetComponent<AudioSource>();
        if (!source)
        {
            source = target.AddComponent<AudioSource>();
        }

        //setting up source
        source.clip = sound.AudioClip;
        source.outputAudioMixerGroup = sound.AudioMixerGroup;
        source.volume = sound.Volume;
        source.pitch = sound.Pitch;
        source.spatialBlend = sound.SpatialBlend;
        source.loop = sound.Loop;

        //playing sound
        source.Play();
    }

    public void SetVolume(string mixerGroupName, float volume)
    {
        if(volume == 0)
        {
            audioMixer.SetFloat(mixerGroupName, -80);
        }
        else
        {
            audioMixer.SetFloat(mixerGroupName, Mathf.Log10(volume) * 20);
        }
    }

    public IEnumerator TransitionWithDelay(Theme theme)
    {
        yield return new WaitForEndOfFrame();
        theme.ThemeBase.Snapshot.TransitionTo(0.5f);
        if (!theme.Playing)
        {
            PlaySound(theme.AudioSourceObj, theme.ThemeBase.SoundName);
            theme.Playing = true;
        }
    }
}
