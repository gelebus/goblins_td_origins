using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Theme
{
    public ThemeBase ThemeBase;
    public GameObject AudioSourceObj;
    public bool Playing;
}
