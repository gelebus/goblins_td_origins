using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "Sound", menuName = "Sound/Create a new Theme")]
public class ThemeBase : ScriptableObject
{
    [SerializeField] private string soundName;
    [SerializeField] private AudioMixerSnapshot snapshot;

    public string SoundName { get { return soundName; } }
    public AudioMixerSnapshot Snapshot { get { return snapshot; } }
}
