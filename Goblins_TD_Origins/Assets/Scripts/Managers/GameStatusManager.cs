using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStatusManager : MonoBehaviour
{
    [Header("Overlays")]
    [SerializeField] private GameObject startLevelUI;
    [SerializeField] private GameObject gameOverUI;
    [SerializeField] private GameObject victoryUI;
    [SerializeField] private GameObject playerStatsUI;
    [SerializeField] private GameObject shopUI;

    [Header("Rounds")]
    [SerializeField] private Text roundsText;
    [SerializeField] private Text roundsShadowText;

    private bool gameStarted;

    private Animator anim;
    private PlayerManager player;

    public static bool GameEnded;

    private void Awake()
    {
        anim = startLevelUI.GetComponent<Animator>();
        GameEnded = false;
        gameStarted = false;
        Time.timeScale = 0;
    }

    private void Start()
    {
        player = PlayerManager.Instance;
    }

    private void Update()
    {
        if (!gameStarted)
            return;

        if (GameEnded)
            return;

        if (player.HeartAmount <= 0)
        {
            ActivateGameOver();
        }

        if (DidPlayerWin())
        {
            ActivateVictory();
        }
    }

    public void PrepareGame()
    {
        anim.SetBool("GameStarted", true);
        playerStatsUI.SetActive(true);
        shopUI.SetActive(true);
        StartCoroutine(StartGame());
    }

    private IEnumerator StartGame()
    {
        Time.timeScale = 1;

        yield return null;

        gameStarted = true;
        yield return new WaitForSeconds(0.4f);
        startLevelUI.SetActive(false);
    }

    private void ActivateGameOver()
    {
        GameEnded = true;

        gameOverUI.SetActive(true);
        StartCoroutine(TurnOffPlayerUIWithDelay());
        shopUI.SetActive(false);

        roundsText.text = player.CurrentWave.ToString();
        roundsShadowText.text = player.CurrentWave.ToString();

        Time.timeScale = 0;
    }

    private bool DidPlayerWin()
    {
        List<GameObject> enemies = SpawnManager.Instance.AllActiveEnemies;

        bool playerIsInLastWave = player.CurrentWave == player.MaxWave;
        bool playerHeartsAreAboveZero = player.HeartAmount > 0;
        bool playerKilledAllEnemies = enemies.Count == 0;

        if (playerIsInLastWave && playerHeartsAreAboveZero && playerKilledAllEnemies && SpawnManager.Instance.LastWaveEnded)
        {
            return true;
        }

        return false;
    }

    private void ActivateVictory()
    {
        GameEnded = true;

        victoryUI.SetActive(true);
        StartCoroutine(TurnOffPlayerUIWithDelay());
        shopUI.SetActive(false);

        Time.timeScale = 0;
    }

    private IEnumerator TurnOffPlayerUIWithDelay()
    {
        yield return new WaitForEndOfFrame();
        playerStatsUI.SetActive(false);
    }
}
