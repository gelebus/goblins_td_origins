using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class OptionsManager : MonoBehaviour
{
    [Header("UI elements")]
    [SerializeField] private GameObject optionsMenuUI;
    [SerializeField] private GameObject audioMenuUI;
    [SerializeField] private GameObject gameMenuUI;
    [SerializeField] private GameObject controlsMenuUI;

    [Header("Game")]
    [SerializeField] private TMP_Dropdown resolutionDropdown;

    [Header("Audio")]
    [SerializeField] private Slider masterVolume;
    [SerializeField] private Slider musicVolume;
    [SerializeField] private Slider sfxVolume;

    private readonly string masterVolumeKey = "masterVolume";
    private readonly string musicVolumeKey = "musicVolume";
    private readonly string sfxVolumeKey = "sfxVolume";

    private List<Resolution> resolutions;

    private void Start()
    {
        ResolutionSetup();

        //checks if previous settings exist
        if (!PlayerPrefs.HasKey(masterVolumeKey))
        {
            PlayerPrefs.SetFloat(masterVolumeKey, 0.8f);
        }
        if (!PlayerPrefs.HasKey(musicVolumeKey))
        {
            PlayerPrefs.SetFloat(musicVolumeKey, 0.8f);
        }
        if (!PlayerPrefs.HasKey(sfxVolumeKey))
        {
            PlayerPrefs.SetFloat(sfxVolumeKey, 0.8f);
        }

        //sets volume to settings
        masterVolume.value = PlayerPrefs.GetFloat(masterVolumeKey);
        musicVolume.value = PlayerPrefs.GetFloat(musicVolumeKey);
        sfxVolume.value = PlayerPrefs.GetFloat(sfxVolumeKey);
    }

    //Gets all possible resolutions + selects the best one for the current user
    private void ResolutionSetup()
    {
        List<string> resolutionOptions = new List<string>();
        int currentResolutionIndex = 0;

        resolutions = new List<Resolution>();

        foreach (Resolution res in Screen.resolutions.Select(resolution => new Resolution { width = resolution.width, height = resolution.height }).Distinct()) 
        {
            resolutions.Add(res);
        }

        resolutionDropdown.ClearOptions();

        for (int i = 0; i < resolutions.Count; i++)
        {
            resolutionOptions.Add($"{resolutions[i].width}x{resolutions[i].height}");
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(resolutionOptions);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }

    //toggle UI elements
    public void DisableOptionsMenu()
    {
        optionsMenuUI.SetActive(false);
    }

    public void ToggleAudioMenu()
    {
        audioMenuUI.SetActive(!audioMenuUI.activeSelf);
    }

    public void ToggleGameMenu()
    {
        gameMenuUI.SetActive(!gameMenuUI.activeSelf);
    }

    public void ToggleControlsMenu()
    {
        controlsMenuUI.SetActive(!controlsMenuUI.activeSelf);
    }

    //audio settings
    public void SetMasterVolume(float volume)
    {
        PlayerPrefs.SetFloat(masterVolumeKey, volume);
        AudioManager.Instance.SetVolume(masterVolumeKey, PlayerPrefs.GetFloat(masterVolumeKey));
    }
    public void SetMusicVolume(float volume)
    {
        PlayerPrefs.SetFloat(musicVolumeKey, volume);
        AudioManager.Instance.SetVolume(musicVolumeKey, PlayerPrefs.GetFloat(musicVolumeKey));
    }
    public void SetGameplayVolume(float volume)
    {
        PlayerPrefs.SetFloat(sfxVolumeKey, volume);
        AudioManager.Instance.SetVolume(sfxVolumeKey, PlayerPrefs.GetFloat(sfxVolumeKey));
    }

    //game settings
    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }
    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }
}
