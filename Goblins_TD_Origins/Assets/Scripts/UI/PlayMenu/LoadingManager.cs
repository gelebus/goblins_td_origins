using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingManager : MonoBehaviour
{
    [SerializeField] private GameObject loadingScreen;
    [SerializeField] private Slider loadingBar;
    [SerializeField] private TextMeshProUGUI loadingText;

    public void LoadLevel(int sceneIndex)
    {
        loadingScreen.GetComponentInParent<Animator>().SetBool("LevelSelected", true);
        loadingScreen.SetActive(true);

        StartCoroutine(ShowLoadingScreen(sceneIndex));
    }

    private IEnumerator ShowLoadingScreen(int sceneIndex)
    {
        yield return new WaitForSeconds(0.4f);
        yield return StartCoroutine(LoadAsync(sceneIndex));
    }

    private IEnumerator LoadAsync(int sceneIndex)
    {
        AsyncOperation asyncOp = SceneManager.LoadSceneAsync(sceneIndex);

        while (!asyncOp.isDone)
        {
            float progress = Mathf.Clamp01(asyncOp.progress / 0.9f);
            loadingBar.value = progress;

            float roundedProgress = Mathf.Floor(progress * 100f);
            loadingText.text = roundedProgress + "%";

            yield return null;
        }
    }
}
