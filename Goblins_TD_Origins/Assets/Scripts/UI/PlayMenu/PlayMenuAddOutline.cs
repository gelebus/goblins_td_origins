using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayMenuAddOutline : MonoBehaviour
{
    public void EnableOutline(Transform mapLvlContainer)
    {
        mapLvlContainer.gameObject.GetComponent<Outline>().enabled = true;
    }
}
