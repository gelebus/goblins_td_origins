using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CustomCursor : MonoBehaviour
{
    [SerializeField] private Texture2D originalCursor;
    [SerializeField] private Texture2D whenClickCursor;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        HandleCursorTexture();
    }

    private void HandleCursorTexture()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            Cursor.SetCursor(whenClickCursor, Vector2.zero, CursorMode.ForceSoftware);
        }
        else
        {
            Cursor.SetCursor(originalCursor, Vector2.zero, CursorMode.ForceSoftware);
        }
    }
}
