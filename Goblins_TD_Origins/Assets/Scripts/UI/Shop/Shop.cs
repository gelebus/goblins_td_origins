using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public static Shop Instance;

    [SerializeField] private TowerBase[] towers;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        gameObject.SetActive(false);
    }

    public bool BuyTower()
    {
        TowerBase tower = BuildManager.Instance.GetTowerToBuild();

        if (tower == null)
        {
            return false;
        }

        foreach (TowerBase towerBase in towers)
        {
            if (tower.TowerInfo.TowerName == towerBase.TowerInfo.TowerName)
            {
                return CanBuyTower(tower.TowerCost);
            }
        }

        return false;
    }

    public bool CanBuyTower(int cost)
    {
        int currentMoneyAmount = PlayerManager.Instance.MoneyAmount;
        int newMoneyAmount = currentMoneyAmount - cost;

        if (0 <= newMoneyAmount)
        {
            PlayerManager.Instance.MoneyAmount = newMoneyAmount;
            return true;
        }

        return false;
    }

    public void SellTower()
    {
        GameObject tower = BuildManager.Instance.GetTowerToSell();
        TowerStats towerStats = tower.GetComponent<TowerStats>();

        //add money
        PlayerManager.Instance.MoneyAmount += towerStats.SellValue;

        //reset tower upgrade changes

        tower.GetComponent<BuffTowersInRange>().DisableBuff();
        tower.GetComponent<TowerModelHandler>().ResetModel();
        for (int i = 0; i < towerStats.UpgradeIndex.Length; i++)
        {
            towerStats.UpgradeIndex[i] = 0;
        }
        towerStats.ReceiveBuffAmount = new List<TowerBuff>();

        //return obj
        ObjectPooler.Instance.ReturnPoolObject(towerStats.TowerInfo.TowerKey, tower);

        BuildManager.Instance.ActivateTowerOptionsFadeOut();
    }

    public void ResetTowerImages(bool resetAll, Image towerImage, Color originalColor)
    {
        Image[] towerImages = TowerListHandler.Instance.AllTowers;
        for (int i = 0; i < towerImages.Length; i++)
        {
            if (towerImages[i] != towerImage || resetAll)
            {
                towerImages[i].color = originalColor;
            }
        }
    }

    public TowerBase[] GetTowerBases()
    {
        return towers;
    }
}
