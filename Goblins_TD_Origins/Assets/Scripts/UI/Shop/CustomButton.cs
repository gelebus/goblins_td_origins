using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CustomButton : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private TowerBase towerBase;
    [SerializeField] private Image towerInfoDisplay;
    [SerializeField] private TextMeshProUGUI towerInfoCamoText;
    [SerializeField] private TextMeshProUGUI towerInfoDamageText;
    [SerializeField] private TextMeshProUGUI towerInfoExplosionRadiusText;
    [SerializeField] private TextMeshProUGUI towerInfoFirerateText;
    [SerializeField] private TextMeshProUGUI towerInfoRangeText;

    private TowerListHandler towerListHandler;
    private BuildManager buildManager;
    private Image towerImage;
    private TextMeshProUGUI towerCostText;

    private void Start()
    {
        buildManager = BuildManager.Instance;
        towerListHandler = TowerListHandler.Instance;
        towerCostText = GetComponentInChildren<TextMeshProUGUI>();
        towerImage = GetComponent<Image>();
        towerCostText.text = towerBase.TowerCost.ToString();

        SetTowerInfo();
    }

    private void SetTowerInfo()
    {
        towerInfoCamoText.text = towerBase.CanTargetCamo.ToString();
        towerInfoDamageText.text = towerBase.Ammo.BaseDamage.ToString();
        towerInfoExplosionRadiusText.text = towerBase.Ammo.ExplosionRadius.ToString();
        towerInfoFirerateText.text = towerBase.FireRate.ToString();
        towerInfoRangeText.text = towerBase.Range.ToString();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Color current = towerImage.color;
        if (current == towerBase.TowerInfo.TowerUI.Original)
        {
            towerImage.color = towerBase.TowerInfo.TowerUI.Hover;
        }

        if (buildManager.GetTowerToBuild() == null)
        {
            towerInfoDisplay.gameObject.SetActive(true);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        TowerCurrentStatsManager.Instance.ShowOrHideTowerCurrentStats(null);

        Shop.Instance.ResetTowerImages(false, towerImage, towerBase.TowerInfo.TowerUI.Original);
        if (GetComponent<DialogueTrigger>())
        {
            GetComponent<DialogueTrigger>().TriggerDialogue();
        }
        ClickHandler();
    }

    private void ClickHandler()
    {
        Color current = towerImage.color;
        if (current == towerBase.TowerInfo.TowerUI.Hover)
        {
            towerInfoDisplay.gameObject.SetActive(false);

            towerImage.color = towerBase.TowerInfo.TowerUI.Selected;
            towerListHandler.SetTowerDisplayName(towerBase.TowerInfo.TowerName);
            buildManager.SetTowerToBuild(towerBase);
            return;
        }
        else
        {
            towerInfoDisplay.gameObject.SetActive(true);

            towerImage.color = towerBase.TowerInfo.TowerUI.Hover;
            towerListHandler.ResetTowerDisplayName();
            buildManager.SetTowerToBuild(null);
            return;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Color current = towerImage.color;
        if (current == towerBase.TowerInfo.TowerUI.Hover)
        {
            towerImage.color = towerBase.TowerInfo.TowerUI.Original;
        }

        towerInfoDisplay.gameObject.SetActive(false);
    }
}