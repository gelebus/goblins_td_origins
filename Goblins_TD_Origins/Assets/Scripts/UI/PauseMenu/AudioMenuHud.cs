using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AudioMenuHud : MonoBehaviour
{
    [SerializeField] private Slider masterVolume;
    [SerializeField] private Slider musicVolume;
    [SerializeField] private Slider sfxVolume;

    [SerializeField] private TextMeshProUGUI masterVolumeText;
    [SerializeField] private TextMeshProUGUI musicVolumeText;
    [SerializeField] private TextMeshProUGUI sfxVolumeText;

    private void Update()
    {
        SetHud();
    }

    void SetHud()
    {
        masterVolumeText.text = Mathf.Floor(masterVolume.value * 100).ToString();
        musicVolumeText.text = Mathf.Floor(musicVolume.value * 100).ToString();
        sfxVolumeText.text = Mathf.Floor(sfxVolume.value * 100).ToString();
    }
}
