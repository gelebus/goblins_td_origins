using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerCurrentStatsManager : MonoBehaviour
{
    [SerializeField] private GameObject towerCurrentStatsUI;

    public static TowerCurrentStatsManager Instance;

    private GameObject prevSelectedTower;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void ShowOrHideTowerCurrentStats(GameObject selectedTower)
    {
        if (selectedTower == null || prevSelectedTower != null && prevSelectedTower.GetInstanceID() == selectedTower.GetInstanceID())
        {
            prevSelectedTower = null;
            towerCurrentStatsUI.GetComponent<Animator>().SetBool("TowerCurrentStatsActive", false);
            StartCoroutine(DelayedDeactivationTCS());
        }
        else
        {
            prevSelectedTower = selectedTower;
            towerCurrentStatsUI.SetActive(true);
            towerCurrentStatsUI.GetComponent<TowerCurrentStatsHUD>().Init(selectedTower);
            towerCurrentStatsUI.GetComponent<Animator>().SetBool("TowerCurrentStatsActive", true);
        }
    }

    private IEnumerator DelayedDeactivationTCS()
    {
        yield return new WaitForSeconds(0.1f);
        towerCurrentStatsUI.SetActive(false);
    }
}
