using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TowerListHandler : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI towerDisplayNameText;
    [SerializeField] private string defaultTurretDisplayName;
    
    [HideInInspector]
    public Image[] AllTowers;

    public static TowerListHandler Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        AllTowers = new Image[transform.childCount];

        for (int i = 0; i < AllTowers.Length; i++)
        {
            AllTowers[i] = transform.GetChild(i).GetComponent<Image>();
        }
    }

    private void Start()
    {
        ResetTowerDisplayName();
    }

    public void SetTowerDisplayName(string name)
    {
        towerDisplayNameText.text = name;
    }

    public void ResetTowerDisplayName()
    {
        towerDisplayNameText.text = defaultTurretDisplayName;
    }
}
