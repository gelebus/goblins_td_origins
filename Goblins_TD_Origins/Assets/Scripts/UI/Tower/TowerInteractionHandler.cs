using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TowerInteractionHandler : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    private List<Color> originalColors;
    private bool leftClicked;
    private bool ableToClick;
    private bool rightClicked;

    private void Start()
    {
        leftClicked = false;
        rightClicked = false;
        ableToClick = true;
    }

    private void Update()
    {
        if (leftClicked)
        {
            leftClicked = false;
            HideHoverColor();
            TowerCurrentStatsManager.Instance.ShowOrHideTowerCurrentStats(null);
            ShowOrHideTowerOptions();
            StartCoroutine(MakeLeftClickable());
        }

        if (rightClicked)
        {
            rightClicked = false;
            HideHoverColor();
            TowerCurrentStatsManager.Instance.ShowOrHideTowerCurrentStats(gameObject);
            BuildManager.Instance.SetTowerToUpgrade(null, null);
            StartCoroutine(MakeLeftClickable());
        }
    }

    private void ShowOrHideTowerOptions()
    {
        if (BuildManager.Instance.GetTowerToUpgrade() != null && BuildManager.Instance.GetTowerToUpgrade().GetInstanceID() == gameObject.GetInstanceID())
        {
            BuildManager.Instance.SetTowerToUpgrade(null, null);
        }
        else
        {
            BuildManager.Instance.SetTowerToUpgrade(gameObject, null);
        }
    }

    private IEnumerator MakeLeftClickable()
    {
        yield return new WaitForSeconds(0.15f);

        ableToClick = true;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (!leftClicked && ableToClick)
            {
                leftClicked = true;
                ableToClick = false;
            }
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (!rightClicked && ableToClick)
            {
                rightClicked = true;
                ableToClick = false;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ShowHoverColor();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        HideHoverColor();
    }

    private void ShowHoverColor()
    {
        GameObject towerToUpgrade = BuildManager.Instance.GetTowerToUpgrade();

        if (towerToUpgrade != null && towerToUpgrade.GetInstanceID() == gameObject.GetInstanceID())
            return;

        Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
        originalColors = new List<Color>();

        for (int i = 0; i < renderers.Length; i++)
        {
            originalColors.Add(renderers[i].material.color);
            renderers[i].material.color = GetNewHoverColor(originalColors[i]);
        }
    }

    private Color GetNewHoverColor(Color orgColor)
    {
        float offset = 0.2f;

        float colorR = orgColor.r - offset;
        if (orgColor.r < 0.5f)
            colorR = orgColor.r + offset;

        float colorG = orgColor.g - offset;
        if (orgColor.g < 0.5f)
            colorG = orgColor.g + offset;

        float colorB = orgColor.b - offset;
        if (orgColor.b < 0.5f)
            colorB = orgColor.b + offset;

        return new Color(colorR, colorG, colorB);
    }

    private void HideHoverColor()
    {
        if (originalColors == null)
            return;

        GameObject towerToUpgrade = BuildManager.Instance.GetTowerToUpgrade();
        Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
        int startValue = 0;

        if (towerToUpgrade != null && towerToUpgrade.GetInstanceID() == gameObject.GetInstanceID())
        {
            originalColors = new List<Color>();

            startValue = 0;

            if (renderers[0].gameObject.layer == 8)
                startValue = 1;

            for (int i = startValue; i < renderers.Length; i++)
            {
                originalColors.Add(renderers[i].material.color);
            }
        }

        startValue = 0;

        if (renderers[0].gameObject.layer == 8)
            startValue = 1;

        for (int i = startValue; i < renderers.Length; i++)
        {
            renderers[i].material.color = originalColors[i - startValue];
        }
    }
}
