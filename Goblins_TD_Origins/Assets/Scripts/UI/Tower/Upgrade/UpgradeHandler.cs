using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeHandler : MonoBehaviour
{
    public static UpgradeHandler Instance;

    [SerializeField] private GameObject[] upgradeImages;

    private GameObject towerToUpgrade;
    private TowerStats towerStats;
    private TowerModelHandler towerModelHandler;

    [HideInInspector]
    public float UpgradeCost;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            gameObject.SetActive(false);
        }
    }

    public void ShowUpgradeOptions(GameObject tower)
    {
        towerToUpgrade = tower;
        towerStats = tower.GetComponent<TowerStats>();
        towerModelHandler = tower.GetComponent<TowerModelHandler>();
        for (int i = 0; i < towerStats.UpgradePaths.Length; i++)
        {
            if (towerStats.IsOnLastUpgrade(i) || !towerStats.IsPathUpgradeable(i) || !towerStats.IsMaxPathUpgradeAllowed() && towerStats.GetActivePathsAmount() == 2 && !towerStats.IsPathUpgradeable(i))
            {
                upgradeImages[i].GetComponent<UpgradeInteractionHandler>().DisableLeftClick();
            }
            else
            {
                upgradeImages[i].GetComponent<UpgradeInteractionHandler>().EnableLeftClick();
            }

            if (!towerStats.IsPathUpgradeable(i) || !towerStats.IsMaxPathUpgradeAllowed() && towerStats.GetActivePathsAmount() == 2 && !towerStats.IsPathUpgradeable(i) || towerStats.UpgradeIndex[i] == 2 && !towerStats.IsMaxPathUpgradeAllowed())
            {
                UpgradeTowerBase upgradeTowerBase = towerStats.UpgradePaths[i].Upgrades[towerStats.UpgradePaths[i].Upgrades.Length - 1];

                upgradeImages[i].GetComponent<UpgradeHud>().Init(upgradeTowerBase, towerStats.UpgradeIndex[i]);
                UpgradeCost = upgradeTowerBase.UpgradeCost;
            }
            else
            {
                upgradeImages[i].GetComponent<UpgradeHud>().Init(towerStats.GetCurrentUpgrade(i), towerStats.UpgradeIndex[i]);
            }
        }
    }

    public void UpgradeTower(GameObject upgrade)
    {
        if (towerToUpgrade == null || upgrade == null || towerStats == null)
        {
            return;
        }

        int pathIndex = -1;
        for (int i = 0; i < upgradeImages.Length; i++)
        {
            if(upgradeImages[i] == upgrade)
            {
                pathIndex = i;
            }
        }

        if(pathIndex == -1 || towerStats.IsOnLastUpgrade(pathIndex) || !towerStats.IsPathUpgradeable(pathIndex))
        {
            return;
        }
        if(!towerStats.IsMaxPathUpgradeAllowed() && towerStats.UpgradeIndex[pathIndex] == 2)
        {
            return;
        }

        UpgradeTowerBase upgradeTowerBase = towerStats.GetCurrentUpgrade(pathIndex);

        if (!Shop.Instance.CanBuyTower(upgradeTowerBase.UpgradeCost))
        {
            return;
        }

        UpdateAreaBlock(towerToUpgrade);

        towerModelHandler.SwapModel(pathIndex, towerStats.UpgradeIndex[pathIndex]);
        towerStats.SetUpgrade(pathIndex, towerStats.UpgradeIndex[pathIndex] + 1);

        BuildManager.Instance.SetTowerToUpgrade(towerToUpgrade, upgrade);

        for (int i = 0; i < towerStats.UpgradePaths.Length; i++)
        {
            if (!towerStats.IsPathUpgradeable(i))
            {
                upgradeImages[i].GetComponent<UpgradeHud>().Init(towerStats.UpgradePaths[i].Upgrades[towerStats.UpgradePaths[i].Upgrades.Length - 1], towerStats.UpgradeIndex[i]);
            }
            else if(!towerStats.IsMaxPathUpgradeAllowed() && towerStats.UpgradeIndex[i] == 2)
            {
                upgradeImages[i].GetComponent<UpgradeHud>().Init(towerStats.UpgradePaths[i].Upgrades[towerStats.UpgradePaths[i].Upgrades.Length - 1], towerStats.UpgradeIndex[i]);
            }
        }

        StartCoroutine(StartImageAnimation(upgrade));
    }

    private void UpdateAreaBlock(GameObject newTower)
    {
        Vector3 turretPos = towerToUpgrade.transform.position;
        float radius = 0.5f;
        int layerMask = LayerMask.GetMask("Placeable");

        Physics.OverlapSphere(turretPos, radius, layerMask)[0].GetComponent<Area>().SetUpgradedTower(newTower);
    }

    public IEnumerator StartImageAnimation(GameObject upgrade)
    {
        upgrade.GetComponent<Animator>().SetBool("UpgradeImageInactive", true);

        yield return new WaitForSeconds(0.25f);

        upgrade.GetComponent<Animator>().SetBool("UpgradeImageInactive", false);

        bool isMaxUpgraded = false; 

        for (int i = 0; i < upgradeImages.Length; i++)
        {
            if (upgradeImages[i] == upgrade)
            {
                isMaxUpgraded = towerStats.IsOnLastUpgrade(i);
                if (!towerStats.IsPathUpgradeable(i))
                {
                    isMaxUpgraded = true;
                    upgradeImages[i].GetComponent<UpgradeHud>().Init(towerStats.UpgradePaths[i].Upgrades[towerStats.UpgradePaths[i].Upgrades.Length - 1], towerStats.UpgradeIndex[i]);
                }
                else if (!towerStats.IsMaxPathUpgradeAllowed() && towerStats.UpgradeIndex[i] == 2)
                {
                    isMaxUpgraded = true;
                    upgradeImages[i].GetComponent<UpgradeHud>().Init(towerStats.UpgradePaths[i].Upgrades[towerStats.UpgradePaths[i].Upgrades.Length - 1], towerStats.UpgradeIndex[i]);
                }
                else
                {
                    upgrade.GetComponent<UpgradeHud>().Init(towerStats.GetCurrentUpgrade(i), towerStats.UpgradeIndex[i]);
                }
            }
        }

        yield return new WaitForSeconds(0.25f);

        if (isMaxUpgraded)
        {
            upgrade.GetComponent<UpgradeInteractionHandler>().DisableLeftClick();
        }
        else
        {
            upgrade.GetComponent<UpgradeInteractionHandler>().EnableLeftClick();
        }
    }

    public GameObject[] GetUpgradeImages()
    {
        return upgradeImages;
    }
}
