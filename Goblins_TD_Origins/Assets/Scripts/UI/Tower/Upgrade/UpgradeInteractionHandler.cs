using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UpgradeInteractionHandler : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private GameObject hoverOverlay;
    [SerializeField] private GameObject disabledOverlay;

    private bool leftClicked = false;
    private bool ableToLeftClick = true;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!leftClicked && ableToLeftClick)
        {
            leftClicked = true;
            ableToLeftClick = false;
        }
    }

    private void Update()
    {
        if (!leftClicked)
            return;

        leftClicked = false;

        if (disabledOverlay.activeSelf)
            return;

        UpgradeHandler.Instance.UpgradeTower(gameObject);
    }

    public void EnableLeftClick()
    {
        ableToLeftClick = true;
    }

    public void DisableLeftClick()
    {
        ableToLeftClick = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (disabledOverlay.activeSelf)
            return;

        hoverOverlay.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (disabledOverlay.activeSelf)
            return;

        hoverOverlay.SetActive(false);
    }

    public void ToggleDisabledOverlay(bool active)
    {
        if(disabledOverlay.activeSelf != active)
            disabledOverlay.SetActive(active);
    }
}
