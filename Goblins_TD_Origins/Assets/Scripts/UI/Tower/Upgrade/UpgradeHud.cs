using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeHud : MonoBehaviour
{
    [SerializeField] private Image upgradeInfoImage;
    [SerializeField] private GameObject disabled;
    [SerializeField] private TextMeshProUGUI upgradeNameText;
    [SerializeField] private TextMeshProUGUI upgradeInfoText;
    [SerializeField] private GameObject upgradeCostBackGround;
    [SerializeField] private GameObject upgradeIndexContainer;
    [SerializeField] private Color upgradeIndexAvailableColor;
    [SerializeField] private Color upgradeIndexBoughtColor;

    [HideInInspector]
    public float UpgradeCost;

    public void Init(UpgradeTowerBase upgradeTurretBase, int upgradeIndex)
    {
        UpgradeCost = upgradeTurretBase.UpgradeCost;

        upgradeInfoImage.gameObject.SetActive(true);
        upgradeCostBackGround.SetActive(true);
        disabled.SetActive(false);

        if (upgradeTurretBase.UpgradeImage == null)
        {
            if(upgradeIndex > 2)
            {
                upgradeNameText.text = upgradeTurretBase.UpgradeTitle;
            }
            else
            {
                upgradeNameText.text = "Path closed";
            }
            upgradeInfoText.text = upgradeTurretBase.UpgradeInfo;

            upgradeCostBackGround.SetActive(false);
            upgradeInfoImage.gameObject.SetActive(false);

            disabled.SetActive(true);
        }
        else
        {
            upgradeInfoImage.sprite = upgradeTurretBase.UpgradeImage;
            upgradeNameText.text = upgradeTurretBase.UpgradeTitle;
            upgradeInfoText.text = upgradeTurretBase.UpgradeInfo;
            upgradeCostBackGround.GetComponentInChildren<TextMeshProUGUI>().text = upgradeTurretBase.UpgradeCost.ToString();
        }

        foreach (Image img in upgradeIndexContainer.GetComponentsInChildren<Image>())
        {
            img.color = upgradeIndexAvailableColor;
        }

        for (int i = 0; i < upgradeIndex; i++)
        {
            upgradeIndexContainer.GetComponentsInChildren<Image>()[i].color = upgradeIndexBoughtColor;
        }
    }
}
