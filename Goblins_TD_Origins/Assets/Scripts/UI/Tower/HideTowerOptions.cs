using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HideTowerOptions : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        BuildManager.Instance.SetTowerToUpgrade(null, null);
        TowerCurrentStatsManager.Instance.ShowOrHideTowerCurrentStats(null);
    }
}
