using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TowerCurrentStatsHUD : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI towerCurrentStatsTitle;
    [SerializeField] private TextMeshProUGUI towerCurrentStatsCamoText;
    [SerializeField] private TextMeshProUGUI towerCurrentStatsDamageText;
    [SerializeField] private TextMeshProUGUI towerCurrentStatsExplosionRadiusText;
    [SerializeField] private TextMeshProUGUI towerCurrentStatsFirerateText;
    [SerializeField] private TextMeshProUGUI towerCurrentStatsRangeText;

    public void Init(GameObject selectedTower)
    {
        TowerStats towerStats = selectedTower.GetComponent<TowerStats>();
        towerCurrentStatsTitle.text = towerStats.TowerInfo.TowerName;
        towerCurrentStatsCamoText.text = towerStats.GetCanTargetCamo().ToString();
        towerCurrentStatsDamageText.text = towerStats.AmmoDamage.ToString();
        towerCurrentStatsExplosionRadiusText.text = towerStats.AmmoExplosionRadius.ToString();
        towerCurrentStatsFirerateText.text = towerStats.FireRate.ToString();
        towerCurrentStatsRangeText.text = towerStats.Range.ToString();
    }
}
