using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue
{
    [SerializeField] private string nameOfPersonSpeaking;
    [TextArea(3,10)]
    [SerializeField] private string[] sentences;

    public string NameOfPersonSpeaking
    {
        get { return nameOfPersonSpeaking; }
    }
    public string[] Sentences
    {
        get { return sentences; }
    }
    public bool Done { get; set; }
}
