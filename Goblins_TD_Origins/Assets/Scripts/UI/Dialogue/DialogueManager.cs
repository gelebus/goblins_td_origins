using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    [SerializeField] private GameObject dialogueBox;
    [SerializeField] private TextMeshProUGUI dialogueText;
    [SerializeField] private TextMeshProUGUI dialogueNameText;

    private Queue<string> sentences;
    private List<int> usedDialogues;

    public static DialogueManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        sentences = new Queue<string>();
        usedDialogues = new List<int>();
    }

    public void StartDialogue(Dialogue dialogue, int dialogueIndex)
    {
        if(usedDialogues.Count > 0)
        {
            for (int i = 0; i < usedDialogues.Count; i++)
            {
                if(dialogueIndex == usedDialogues[i])
                {
                    return;
                }
            }
        }
        if (dialogue.Done)
        {
            return;
        }
        usedDialogues.Add(dialogueIndex);
        Time.timeScale = 0f;
        sentences.Clear();

        foreach(string sentence in dialogue.Sentences)
        {
            sentences.Enqueue(sentence);
        }
        dialogueNameText.text = dialogue.NameOfPersonSpeaking;
        DisplayNextSentence();
        dialogueBox.SetActive(true);
    }

    public void DisplayNextSentence()
    {
        if(sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    private IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach(char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    private void EndDialogue()
    {
        dialogueBox.SetActive(false);
        Time.timeScale = 1f;
    }
}
