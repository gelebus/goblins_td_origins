using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    [SerializeField] private Dialogue dialogue;
    [SerializeField] private int dialogueIndex;
    private DialogueManager DialogueManager;
    private void Start()
    {
        DialogueManager = DialogueManager.Instance;
    }

    public void TriggerDialogue()
    {
        DialogueManager.StartDialogue(dialogue, dialogueIndex);
        dialogue.Done = true;
    }
}
