using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuButtonManager : MonoBehaviour
{
    [SerializeField] private GameObject optionsMenuUI;

    public void OptionsButton()
    {
        optionsMenuUI.SetActive(true);
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
